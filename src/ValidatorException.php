<?php

declare(strict_types=1);

namespace Yunik;




class ValidatorException extends BaseException {

	protected $missingFields = [];

	public function __construct(array $missingFields, string $message = 'Internal Error', int $code = 0)
	{
		$this->missingFields = $missingFields;
		parent::__construct($message, $code);
	}

	static function throw(array $missingFields)
	{
		throw new ValidatorException($missingFields);
	}

	static function response(ValidatorException $ex)
	{
		return rest_ensure_response([
			'error' => true,
			'fields' => $ex->getMissingFields()
		]);
	}

	public function getMissingFields() : array
	{
		return $this->missingFields;
	}
}
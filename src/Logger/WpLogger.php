<?php

declare(strict_types=0); // There's a bug in fwrite

namespace Yunik\Logger;




/**
 * Logger implementation to write to a file
 */
class WpLogger extends AbstractLogger implements \Yunik\Interfaces\LoggerInterface {
	

	public static function log($message, \Exception $ex = null) : void
	{
		self::write($message, 'log');
	}

	public static function debug($message, \Exception $ex = null) : void
	{
		self::write($message, 'debug');
	}

	public static function error($message, \Exception $ex = null) : void
	{
		self::write($message, 'error');
	}

	private static function write($message, $level = 'log') : void
	{
		$message = strtoupper($level) . ': ' . $message;
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
	}
}
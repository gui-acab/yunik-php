<?php

declare(strict_types=0); // There's a bug in fwrite

namespace Yunik\Logger;




/**
 * Logger implementation to write to a file
 */
class TextFileLogger extends AbstractLogger implements \Yunik\Interfaces\LoggerInterface {
	
	public static $filePath;

	public static function config($filePath)
	{
		self::$filePath = $filePath;
	}

	public static function log($message, \Exception $ex = null) : void
	{
		self::write($message, 'log');
	}

	public static function debug($message, \Exception $ex = null) : void
	{
		self::write($message, 'debug');
	}

	public static function error($message, \Exception $ex = null) : void
	{
		self::write($message, 'error');
	}

	private static function write($message, $level = 'log') : void
	{
		$output = strtoupper($level) . ': ' . $message . PHP_EOL;
		file_put_contents(self::$filePath, $output);
	}
}
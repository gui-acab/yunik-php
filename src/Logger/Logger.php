<?php

declare(strict_types=1);

namespace Yunik\Logger;




/**
 * Logger implementation to write to console
 */
class Logger extends AbstractLogger implements \Yunik\Interfaces\LoggerInterface {

	/**
	 * Config the Logger
	 */
	public static function config() : void
	{

	}
	
	/**
	 * Log a regular message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function log($message, \Exception $ex = null) : void
	{
		self::write($message, 'log');
	}

	/**
	 * Log a debug message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function debug($message, \Exception $ex = null) : void
	{
		self::write($message, 'debug');
	}

	/**
	 * Log a error message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function error($message, \Exception $ex = null) : void
	{
		self::write($message, 'error');
	}

	/**
	 * Helper method to write the log
	 * @param  string $message the message
	 * @param  string $level   the log level
	 */
	private static function write($message, $level = 'debug') : void
	{
		$body = strtoupper($level) . ': ' . $message;
		fwrite(STDERR, print_r($body, TRUE) . PHP_EOL);
	}
}
<?php

declare(strict_types=1);

namespace Yunik\Logger;




/**
 * Logger implementation to write to console
 */
class YunikLogger extends AbstractLogger implements \Yunik\Interfaces\LoggerInterface {

	public static $logger;

	/**
	 * Config the Logger
	 */
	public static function config($logger) : void
	{
		self::$logger = $logger;
	}
	
	/**
	 * Log a regular message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function log($message, \Exception $ex = null) : void
	{
		self::$logger->log($message, $ex);
	}

	/**
	 * Log a debug message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function debug($message, \Exception $ex = null) : void
	{
		self::$logger->debug($message, $ex);
	}

	/**
	 * Log a error message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	public static function error($message, \Exception $ex = null) : void
	{
		self::$logger->error($message, $ex);
	}
}
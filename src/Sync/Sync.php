<?php

namespace Yunik\Sync;

use Yunik\Domain\ListOperationResult,
	Yunik\Domain\WpProductDto,
	Yunik\Domain\WpCategoryDto,
	Yunik\Logger\YunikLogger,
	Yunik\Db\LocalPdo,
	Yunik\Interfaces\SyncInterface;




class Sync {

	// Categories queries
	const QUERY_INSERT = 'INSERT INTO yunik_queue (list, item_id, value, until, created, status) VALUES(:list, :item_id, :value, :until, SYSDATE(), :status)';

	const QUERY_SELECT = 'SELECT item_id, list, item_id, value, until, created FROM yunik_queue WHERE list=:list AND item_id=:item_id';

	const QUERY_EXISTS = 'SELECT id FROM yunik_queue WHERE item_id=:item_id AND list=:list';

	const QUERY_SELECT_LIST = 'SELECT id, list, item_id, value, until, created FROM yunik_queue WHERE list=:list LIMIT :take OFFSET :skip';

	const QUERY_SELECT_AVAILABLE_LIST = 'SELECT id, list, item_id, value, until, created FROM yunik_queue WHERE until <= DATE(NOW()) AND list=:list LIMIT :take OFFSET :skip';

	const QUERY_COUNT_LIST = 'SELECT count(*) FROM yunik_queue WHERE list=:list';

	const QUERY_REMOVE = 'DELETE FROM yunik_queue WHERE list=:list AND item_id=:item_id';

	const QUERY_REMOVE_KEY = 'DELETE FROM yunik_queue WHERE item_id=:item_id'; 

	const QUERY_REMOVE_LIST = 'DELETE FROM yunik_queue WHERE list=:list';

	const QUERY_UPDATE_STATUS = 'UPDATE yunik_queue SET status=:status WHERE list=:list AND item_id=:item_id';

	public function exists(string $list, string $itemId) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_EXISTS);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':item_id', $itemId);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;
		return $rows >= 1;
	}

	public function updateStatus(string $list, string $itemId)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_UPDATE_STATUS);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':item_id', $itemId);
		$res = $stmt->execute();

		$pdo = null;

		if(!$res) {
			YunikLogger::error("Error updating queue item status: " . print_r($res, true));
			return false;
		}

		return true;
	}

	public function insert(string $list, string $itemId, string $value, $until = null)
	{
		if($until == null) {
			$until = date("Y-m-d H:i:s", strtotime('now'));
		}
		$status = 1;
		
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_INSERT);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':item_id', $itemId);
		$stmt->bindParam(':value', $value);
		$stmt->bindParam(':until', $until, LocalPdo::PARAM_STR);
		$stmt->bindParam(':status', $status);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		$pdo = null;

		if(!$res) {
			YunikLogger::error("Error creating a new queue item: " . print_r($res, true));
			return false;
		}

		return intval($id);
	}

	public function remove(string $list, string $itemId) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_REMOVE);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':item_id', $itemId);

		$res = $stmt->execute();
		$pdo = null;
		if(!$res) {
			YunikLogger::error("Error removing a queue item: " . print_r($res, true));
			return false;
		}

		return true;
	}

	public function removeList(string $list) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_REMOVE_LIST);
		$stmt->bindParam(':list', $list);

		$res = $stmt->execute();
		$pdo = null;
		if(!$res) {
			YunikLogger::error("Error removing a queue item: " . print_r($res, true));
			return false;
		}

		return true;
	}

	public function get(string $list, string $itemId)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':item_id', $itemId);
		
		$stmt->execute();
		$map = $stmt->fetch();
		$pdo = null;

		return $map;
	}

	public function query($list, $skip = 0, $take = 20)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_LIST);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':skip', $skip);
		$stmt->bindParam(':take', $take);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}

	public function queryExpired($list, $skip = 0, $take = 20)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_AVAILABLE_LIST);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':skip', $skip);
		$stmt->bindParam(':take', $take);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}
}
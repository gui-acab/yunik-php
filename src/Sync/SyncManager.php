<?php

namespace Yunik\Sync;

use Yunik\Domain\ListOperationResult,
	Yunik\Domain\WpProductDto,
	Yunik\Domain\WpCategoryDto,
	Yunik\Logger\YunikLogger,
	Yunik\Db\LocalPdo,
	Yunik\Interfaces\SyncInterface;




class SyncManager implements SyncInterface {

	// Categories queries
	const QUERY_INSERT_LIST = 'INSERT INTO rcsoft_list (categoryId, name, list, created, parent) VALUES(:categoryId, :name, :list, SYSDATE(), :parent)';

	const QUERY_SELECT_LIST = 'SELECT id, categoryId, name, list, parent, created FROM rcsoft_list WHERE list=:list LIMIT :take OFFSET :skip';

	const QUERY_SELECT_CATEGORY = 'SELECT id, categoryId, externalId, list FROM rcsoft_list WHERE id=:id AND list=:list';

	const QUERY_SELECT_CATEGORY_PENDING = 'SELECT id, categoryId, parent, list, name FROM rcsoft_list WHERE categoryId=:categoryId';// AND list="pending" 

	const QUERY_EXISTS_CATEGORY = 'SELECT count(*) FROM rcsoft_list WHERE categoryId=:id AND list=:list';

	const QUERY_COUNT_CATEGORY = 'SELECT count(*) FROM rcsoft_list WHERE list=:list';

	const QUERY_REMOVE_CATEGORY = 'DELETE FROM rcsoft_list WHERE categoryId=:categoryId'; 

	const QUERY_REMOVE_ALL_CATEGORIES = 'DELETE FROM rcsoft_list WHERE list=:list';

	// Product queries
	const QUERY_INSERT_PRODUCT = 'INSERT INTO rcsoft_products (stepValue, externalId, list, created, sku, price, promo, promoStart, promoEnd, stock, category, obs, brand, weight, promoCat, sort) VALUES(:stepValue, :externalId, :list, SYSDATE(), :sku, :price, :promo, :promoStart, :promoEnd, :stock, :category, :obs, :brand, :weight, :promoCat, :sort)';

	const QUERY_SELECT_PRODUCT_LIST = 'SELECT id, externalId, list, sku, price, promo, created, stock, category, stepValue, brand, weight, promoCat, sort FROM rcsoft_products WHERE list=:list ORDER BY price ASC LIMIT :take OFFSET :skip';

	const QUERY_SELECT_PRODUCT = 'SELECT id, externalId, list, sku, price, promo, promoStart, promoEnd, stock, created, stepValue, brand, promoCat, sort FROM rcsoft_products WHERE id=:id';

	const QUERY_SELECT_PRODUCT_UNIQUE = 'SELECT id, externalId, list, sku, price, promo, stock, created, stepValue FROM rcsoft_products WHERE (list=:list OR :listNull IS NULL) AND sku=:sku AND price=:price AND (promo=:pricePromo OR :pricePromoNull IS NULL) AND(promoStart=:promoStart OR :promoStartNull IS NULL) AND (promoEnd=:promoEnd OR :promoEndNull IS NULL) AND (category=:category OR :categoryNull IS NULL) AND stock=:stock AND (stepValue=:stepValue OR :stepValueNull IS NULL) AND (brand=:brand OR :brandNull IS NULL)';

	const QUERY_EXISTS_PRODUCT = 'SELECT count(*) FROM rcsoft_products WHERE externalId=:externalId AND list=:list';

	const QUERY_COUNT_PRODUCT = 'SELECT count(*) FROM rcsoft_products WHERE list=:list';

	const QUERY_REMOVE_PRODUCT = 'DELETE FROM rcsoft_products WHERE externalId=:externalId AND list=:list'; 

	const QUERY_REMOVE_ALL_PRODUCTS = 'DELETE FROM rcsoft_products WHERE list=:list';

	const QUERY_COUNT_PRODUCT_MAP = 'SELECT count(*) FROM rcsoft_map WHERE woocommerce=:woocommerce AND rcsoft=:rcsoft';

	const QUERY_COUT_PRODUCT_MAP_EXIST = 'SELECT count(*) FROM rcsoft_products WHERE externalId=:externalId';

	const QUERY_COUNT_PRODUCT_RCSOFT_MAP = 'SELECT count(*) FROM rcsoft_map WHERE rcsoft=:rcsoft';

	const QUERY_SELECT_PRODUCT_MAP = 'SELECT id, woocommerce, rcsoft FROM rcsoft_map WHERE id=:id';

	const QUERY_SELECT_PRODUCT_WOOCOMMERCE_MAP = 'SELECT id, woocommerce, rcsoft FROM rcsoft_map WHERE woocommerce=:woocommerce';

	const QUERY_SELECT_PRODUCT_RCSOFT_MAP = 'SELECT id, woocommerce, rcsoft FROM rcsoft_map WHERE rcsoft=:rcsoft';

	const QUERY_REMOVE_PRODUCT_MAP = 'DELETE FROM rcsoft_map WHERE woocommerce=:woocommerce AND rcsoft=:rcsoft';

	const QUERY_INSERT_PRODUCT_MAP = 'INSERT INTO rcsoft_map (woocommerce, rcsoft) VALUES(:woocommerce, :rcsoft)';

	// Lists keys
	const LIST_FAILED = 'failed';

	const LIST_PENDING = 'pending';

	/**
	 * Check if a product exists. It's the product, not the map
	 * @param  int    $woocommerce woocommerce product id
	 * @param  int    $rcsoft      rcsoft product id
	 * @return bool              true if exists, false otherwise
	 */
	public function productExists(string $rcsoft) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_COUT_PRODUCT_MAP_EXIST);
		$stmt->bindParam(':externalId', $rcsoft);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;
		return $rows >= 1;
	}

	/**
	 * Check if a map exists between a Woocommerce product and RCSoft
	 * @param  int    $woocommerce woocommerce product id
	 * @param  int    $rcsoft      rcsoft product id
	 * @return bool              true if exists, false otherwise
	 */
	public function productMapExists(int $woocommerce, string $rcsoft) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_COUNT_PRODUCT_MAP);
		$stmt->bindParam(':woocommerce', $woocommerce);
		$stmt->bindParam(':rcsoft', $rcsoft);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;
		return $rows === 1;
	}

	/**
	 * Check if a map exists for a specific RCSoft product
	 * @param  int    $rcsoft      rcsoft product id
	 * @return bool              true if exists, false otherwise
	 */
	public function productMapExistsByRcsoft(string $rcsoft) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_COUNT_PRODUCT_RCSOFT_MAP);
		$stmt->bindParam(':rcsoft', $rcsoft);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;

		return $rows === 1;
	}

	/**
	 * Create a product map
	 * @param int $woocommerce woocommerce product id
	 * @param int $rcsoft      rcsoft product id
	 * @return bool|int false if failed, the id generated for the map otherwise
	 */
	public function addProductMap(?string $woocommerce, string $rcsoft)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_INSERT_PRODUCT_MAP);
		$stmt->bindParam(':woocommerce', $woocommerce);
		$stmt->bindParam(':rcsoft', $rcsoft);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		$pdo = null;

		if(!$res) {
			YunikLogger::error("Error creating a new product map for Woocommerce $woocommerce RCSoft $rcsoft");
			return false;
		}

		return intval($id);
	}

	/**
	 * Remove a product map
	 * @param  int    $woocommerce woocommerce id
	 * @param  int    $rcsoft      rcsoft id
	 */
	public function removeProductMap(int $woocommerce, int $rcsoft) : bool
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_REMOVE_PRODUCT_MAP);
		$stmt->bindParam(':woocommerce', $woocommerce);
		$stmt->bindParam(':rcsoft', $rcsoft);

		$res = $stmt->execute();
		$pdo = null;
		if(!$res) {
			YunikLogger::error("Error removing the product map for Woocommerce $woocommerce RCSoft $rcsoft");
			return false;
		}

		return true;
	}

	/**
	 * Get a product map by his id
	 * @param  int    $mapId the map id
	 * @return array|bool        the product map, false if not exists
	 */
	public function getProductMap(int $mapId)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT_MAP);
		$stmt->bindParam(':id', $mapId);
		
		$stmt->execute();
		$map = $stmt->fetch();
		$pdo = null;

		return $map;
	}

	/**
	 * Get a product map by the woocommerce product id
	 * @param  int    $woocommerce woocommerce product ud
	 * @return array              the product map
	 */
	public function getProductMapByWoocommerce(int $woocommerce) : array
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT_WOOCOMMERCE_MAP);
		$stmt->bindParam(':woocommerce', $woocommerce);
		
		$stmt->execute();
		$map = $stmt->fetch();
		$pdo = null;

		return $map ?: [];
	}

	/**
	 * Get a product map by the rcsoft product id
	 * @param  int    $rcsoft rcsoft product id
	 * @return array         the product map
	 */
	public function getProductMapByRcsoft(string $rcsoft) : array
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT_RCSOFT_MAP);
		$stmt->bindParam(':rcsoft', $rcsoft);
		
		$stmt->execute();
		$map = $stmt->fetch();
		$pdo = null;

		return $map;
	}

	/**
	 * Clear all lists
	 * 
	 * Removes products from Pending and Failed list
	 * Usefull to reset the lists
	 * The removal of all records in a list can't be undo
	 */ 
	public function clearProductLists() : void
	{
		$pdo = new LocalPdo();
		foreach ([self::LIST_PENDING, self::LIST_FAILED] as $list) {
			$stmt = $pdo->prepare(self::QUERY_REMOVE_ALL_PRODUCTS);
			$stmt->bindParam(':list', $list);
			$res = $stmt->execute();
		}
		$pdo = null;
	}

	/*+
	 * Count the products that belong to a list
	 * @return array the total of products in the map
	 */
	public function countProducts(string $list) : int
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_COUNT_PRODUCT);
		$stmt->bindParam(':list', $list);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;
		return $rows;
	}

	/**
	 * Get a product from the Queue by the Queue item Id
	 * @return array the queue item
	 */
	public function getProductFromQueue(int $queueId) : array
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT);
		$stmt->bindParam(':id', $queueId);
		$stmt->execute();
		$rows = $stmt->fetch();

		$pdo = null;
		return $rows;
	}

	/**
	 * Indicates if a product exists in the Pending list
	 * @return bool true if product is the pending queue, false otherwise
	 */ 
	public function productIsPending($externalId) : bool
	{
		return $this->productExistsInList($externalId, self::LIST_PENDING);
	}

	/**
	 * Indicates if a product exists in the Failed list
	 * @return bool true if the product is in the failed queue, false otherwise
	 */ 
	public function productIsFailed($externalId) : bool
	{
		return $this->productExistsInList($externalId, self::LIST_FAILED);
	}

	/**
	 * Compares a product agains a Queue item to check if any changes happen
	 * Used to know when to update or not a product/category
	 * @return bool true if the product is equal, false otherwise
	 */ 
	public function productIsEqual(WpProductDto $product, $list = null) : bool
	{
		$pdo = new LocalPdo();
		$id = $product->getId();
		$sku = $product->getSku();
		$stock = $product->getStock();
		$price = $product->getPrice();
		$pricePromo = $product->getPricePromo();
		$promoStart = $product->getPromoStart();
		$promoEnd = $product->getPromoEnd();
		$category = $product->getCategory();
		$stepValue = $product->getStepValue();
		$brand = $product->getBrand();

		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT_UNIQUE);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':listNull', $list);
		$stmt->bindParam(':sku', $sku);
		$stmt->bindParam(':stock', $stock);
		$stmt->bindParam(':price', $price);
		$stmt->bindParam(':pricePromo', $pricePromo);
		$stmt->bindParam(':pricePromoNull', $pricePromo);
		$stmt->bindParam(':promoStart', $promoStart);
		$stmt->bindParam(':promoStartNull', $promoStart);
		$stmt->bindParam(':promoEnd', $promoEnd);
		$stmt->bindParam(':promoEndNull', $promoEnd);
		$stmt->bindParam(':category', $category);
		$stmt->bindParam(':categoryNull', $category);
		$stmt->bindParam(':stepValue', $stepValue);
		$stmt->bindParam(':stepValueNull', $stepValue);
		$stmt->bindParam(':brand', $brand);
		$stmt->bindParam(':brandNull', $brand);

		try {
			$stmt->execute();
			$rows = $stmt->fetchAll();
			$pdo = null;
			return count($rows) === 1;	
		} catch(\Exception $ex) {
			die(print_r($stepValue));
		}
		
	}

	/**
	 * Indicates if the product exists in a specific list
	 * @return bool true if the product exists in the specified queue, false otherwise
	 */ 
	protected function productExistsInList($externalId, $list)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_EXISTS_PRODUCT);
		$stmt->bindParam(':externalId', $externalId);
		$stmt->bindParam(':list', $list);

		$res = $stmt->execute();

	    $rows = $stmt->fetchColumn();
	    $pdo = null;

	    return $rows >= 1;
	}

	/**
	 * Return a list of products waiting to be created at WP
	 * 
	 * When a new product is found it's added as Pending
	 * When the product is created, it's removed from the Pending list
	 * @return array list of products existing in the pending queue
	 */ 
	public function getPendingProducts($skip = 0, $take = 20) : array
	{
		$res = $this->getListProducts(self::LIST_PENDING, $skip, $take);
		if(!is_array($res) && !$res) {
			YunikLogger::error("Error retrieving the list of pending products.");
			return false;
		}

		return $res;
	}

	/**
	 * Return a list of products that weren't added to WP
	 * 
	 * When for some reason a product can't be created at WP
	 * its added to failed list.
	 * @return bool list of products existing in the failed queue
	 */ 
	public function getFailedProducts($skip = 0, $take = 20) : array
	{
		$res = $this->getListProducts(self::LIST_FAILED, $skip, $take);
		if(!is_array($res) && !$res) {
			YunikLogger::error("Error retrieving the list of failed products.");
			return false;
		}

		return $res;
	}

	/**
	 * Return the list of products from a specific list
	 */ 
	protected function getListProducts($list, $skip = 0, $take = 20)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_PRODUCT_LIST);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':skip', $skip);
		$stmt->bindParam(':take', $take);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}

	/**
	 * Remove a product from the Pending list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function removeProductFromPending(string $externalId) : ListOperationResult
	{
		$res = $this->removeProductFromList($externalId, self::LIST_PENDING);
		if(!$res) {
			YunikLogger::error("Error removing the product {$externalId} to Pending list.");
			return ListOperationResult::error($externalId, $externalId);
		}

		return ListOperationResult::success($externalId, $externalId);
	}

	/**
	 * Remove a product from the Failed list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function removeProductFromFailed(string $externalId) : ListOperationResult
	{
		$res = $this->removeProductFromList($externalId, self::LIST_FAILED);
		if(!$res) {
			YunikLogger::error("Error removing the product {$externalId} to Failed list.");
			return ListOperationResult::error($externalId, $externalId);
		}

		return ListOperationResult::success($externalId, $externalId);
	}

	/**
	 * Removes a product from a specific list
	 * 
	 * Helper method to handle different remove from list operations
	 */ 
	protected function removeProductFromList($externalId, $list)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_REMOVE_PRODUCT);
		$stmt->bindParam(':externalId', $externalId);
		$stmt->bindParam(':list', $list);
		$res = $stmt->execute();

		$pdo = null;
		return $res;
	}

	/**
	 * Add a product to the Pending list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function addProductToPending(WpProductDto $product) : ListOperationResult
	{
		$id = $product->getId();
		$res = $this->addProductToList($product, self::LIST_PENDING);
		if(!$res) {
			YunikLogger::error("Error adding the product {$id} to Pending list.");
			return ListOperationResult::error(null, $id);
		}

		return ListOperationResult::success(null, $id, $res);
	}

	/**
	 * Add a product to the Failed list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function addProductToFailed(WpProductDto $product) : ListOperationResult
	{
		$id = $product->getId();
		$exists = $this->productIsFailed($id);
		if($exists) {
			$this->removeProductFromPending($id);
			YunikLogger::debug("Removed the product {$id} from the Failed list.");
		} else {
			YunikLogger::log("Adding the product {$id} to the Failed list but doesn't exists in the Pending list.");
		} 

		$res = $this->addProductToList($product, self::LIST_FAILED);
		if(!$res) {
			YunikLogger::error("Error adding the product {$id} to Failed list.");
			return ListOperationResult::error(null, $id);
		}

		return ListOperationResult::success(null, $id, $res);
	}

	/**
	 * Add a product to a specific list
	 */ 
	protected function addProductToList(WpProductDto $product, string $list)
	{
		$pdo = new LocalPdo();
		$productId = $product->getId();
		$sku = $product->getSku();
		$stock = $product->getStock();
		$price = $product->getPrice();
		$pricePromo = $product->getPricePromo();
		$promoStart = $product->getPromoStart();
		$promoEnd = $product->getPromoEnd();
		$category = $product->getCategory();
		$stepValue = $product->getStepValue();
		$brand = $product->getBrand();
		$obs = 'Created at ' . date("Y-m-d H:i:s");
		$weight = $product->getWeight();
		$promoCat = $product->getPromoCat();
		$order = $product->getOrder();

		$stmt = $pdo->prepare(self::QUERY_INSERT_PRODUCT);
		
		$stmt->bindParam(':externalId', $sku);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':sku', $sku);
		$stmt->bindParam(':stock', $stock);
		$stmt->bindParam(':price', $price);
		$stmt->bindParam(':promo', $pricePromo);
		$stmt->bindParam(':promoStart', $promoStart);
		$stmt->bindParam(':promoEnd', $promoEnd);
		$stmt->bindParam(':category', $category);
		$stmt->bindParam(':stepValue', $stepValue);
		$stmt->bindParam(':brand', $brand);
		$stmt->bindParam(':obs', $obs);
		$stmt->bindParam(':weight', $weight);
		$stmt->bindParam(':promoCat', $promoCat);
		$stmt->bindParam(':sort', $order);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		
		$pdo = null;
		if(!$res) {
			return false;
		}

		return $id;
	}

	/**
	 * Mark the product as created at WP
	 * 
	 * The product is removed from the Pending or Failed list
	 * @return ListOperationResult the operation result
	 */ 
	public function markProductAsCreated($externalId) : ListOperationResult
	{
		$res = $this->removeProductFromPending($externalId);
		if(!$res) {
			YunikLogger::error("Error removing the product {$productId} to Pending list.");
			return ListOperationResult::error($externalId);
		}

		$res = $this->removeProductFromFailed($externalId);
		if(!$res) {
			YunikLogger::error("Error adding the product {$productId} to Failed list.");
			return ListOperationResult::error($externalId);
		}

		return ListOperationResult::success($externalId);
	}

	/**
	 * Mark the product as Failed at WP
	 * 
	 * The product is removed from the Pending list
	 * and added to Failed list
	 * @return ListOperationResult the operation result
	 */ 
	public function markProductAsFailed($externalId) : ListOperationResult
	{
		$isPending = $this->productIsPending($externalId);
		if($isPending) {
			$this->removeProductFromPending($externalId);
			YunikLogger::debug("Removed the product {$externalId} from the Pending list.");
		}
		
		$product = new WpProductDto($externalId);
		$this->addProductToFailed($product);

		return ListOperationResult::success(null, $externalId);
	}

	/**
	 * Clear all lists
	 * 
	 * Removes categories from Pending and Failed list
	 * Usefull to reset the lists
	 * The removal of all records in a list can't be undo
	 */ 
	public function clearCategoryLists() : void
	{
		$pdo = new LocalPdo();
		foreach ([self::LIST_PENDING, self::LIST_FAILED] as $list) {
			$stmt = $pdo->prepare(self::QUERY_REMOVE_ALL_CATEGORIES);
			$stmt->bindParam(':list', $list);
			$res = $stmt->execute();
		}
		$pdo = null;
	}

	/*+
	 * Count the categories that belong to a list
	 * @return int the total of categories in the specified list
	 */
	public function countCategories($list) : int
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_COUNT_CATEGORY);
		$stmt->bindParam(':list', $list);
		$res = $stmt->execute();
		$rows = $stmt->fetchColumn();

		$pdo = null;
		return $rows;
	}

	/**
	 * Indicates if a category exists in the Pending list
	 * @return bool true if the category is in the pending queue, false otherwise
	 */ 
	public function categoryIsPending($externalId) : bool
	{
		return $this->categoryExistsInList($externalId, self::LIST_PENDING);
	}

	/**
	 * Indicates if a category exists in the Failed list
	 * @return boool true if the category is in the failed queue, false otherwise
	 */ 
	public function categoryIsFailed($externalId) : bool
	{
		return $this->categoryExistsInList($externalId, self::LIST_FAILED);
	}

	/**
	 * Indicates if the category exists in a specific list
	 * @return bool true if the category is in the specified queue list, false otherwise
	 */ 
	protected function categoryExistsInList($externalId, $list) : bool
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_EXISTS_CATEGORY);
		$stmt->bindParam(':id', $externalId);
		$stmt->bindParam(':list', $list);

		$res = $stmt->execute();

	    $rows = $stmt->fetchColumn();
	    $pdo = null;

	    return $rows === 1;
	}

	/**
	 * Return a list of categories waiting to be created at WP
	 * 
	 * When a new category is found it's added as Pending
	 * When the category is created, it's removed from the Pending list
	 * @return array the list of categories in the pending queue
	 */ 
	public function getPendingCategories($skip = 0, $take = 20) : array
	{
		$res = $this->getListCategories(self::LIST_PENDING, $skip, $take);
		if(!is_array($res) && !$res) {
			YunikLogger::error("Error retrieving the list of pending categories.");
			return false;
		}

		return $res;
	}

	/**
	 * Return a list of categories that weren't added to WP
	 * 
	 * When for some reason a category can't be created at WP
	 * its added to failed list.
	 * @return array the list of categories in the pending queue
	 */ 
	public function getFailedCategories($skip = 0, $take = 20) : array
	{
		$res = $this->getListCategories(self::LIST_FAILED, $skip, $take);
		if(!is_array($res) && !$res) {
			YunikLogger::error("Error retrieving the list of failed categories.");
			return false;
		}

		return $res;
	}

	/**
	 * Return the list of categories from a specific list
	 */ 
	protected function getListCategories($list, $skip = 0, $take = 20)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_LIST);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':skip', $skip);
		$stmt->bindParam(':take', $take);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}

	public function getPendingCategory($id)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_SELECT_CATEGORY_PENDING);
		$stmt->bindParam(':categoryId', $id);

		$stmt->execute();
		$res = $stmt->fetch();

		$pdo = null;
		return $res;
	}

	/**
	 * Remove a category from the Pending list
	 * @return ListOperationResult the operation result
	 */ 
	public function removeCategoryFromPending(string $externalId) : ListOperationResult
	{
		$res = $this->removeCategoryFromList(null, $externalId, self::LIST_PENDING);
		if(!$res) {
			YunikLogger::error("Error removing the category {$externalId} to Pending list.");
			return ListOperationResult::error(null, $externalId);
		}

		return ListOperationResult::success(null, $externalId);
	}

	/**
	 * Remove a category from the Failed list
	 * @return ListOperationResult the operation result
	 */ 
	public function removeCategoryFromFailed(string $externalId) : ListOperationResult
	{
		$res = $this->removeCategoryFromList(null, $externalId, self::LIST_FAILED);
		if(!$res) {
			YunikLogger::error("Error removing the category {$externalId} to Failed list.");
			return ListOperationResult::error(null, $externalId);
		}

		return ListOperationResult::success(null, $externalId);
	}

	/**
	 * Removes a category from a specific list
	 * 
	 * Helper method to handle different remove from list operations
	 */ 
	protected function removeCategoryFromList($categoryId, $externalId, $list)
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_REMOVE_CATEGORY);
		$stmt->bindParam(':categoryId', $externalId);
		$res = $stmt->execute();

		$pdo = null;
		return $res;
	}

	/**
	 * Add a category to the Pending list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function addCategoryToPending(WpCategoryDto $category) : ListOperationResult
	{
		$externalId = $category->getId();
		$res = $this->addCategoryToList($category, self::LIST_PENDING);
		if(!$res) {
			YunikLogger::error("Error adding the category {$categoryId} to Pending list.");
			return ListOperationResult::error(null, $externalId);
		}

		return ListOperationResult::success(null, $externalId);
	}

	/**
	 * Add a category to the Failed list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function addCategoryToFailed($categoryId) : ListOperationResult
	{
		$exists = $this->categoryIsFailed($categoryId);
		if($exists) {
			$this->removeCategoryFromPending($categoryId);
			YunikLogger::debug("Removed the category {$categoryId} from the Failed list.");
		} else {
			YunikLogger::log("Adding the category {$categoryId} to the Failed list but doesn't exists in the Pending list.");
		} 

		$category = new WpCategoryDto($categoryId);
		$res = $this->addCategoryToList($category, self::LIST_FAILED);
		if(!$res) {
			YunikLogger::error("Error adding the category {$categoryId} to Failed list.");
			return ListOperationResult::error(null, $categoryId);
		}

		return ListOperationResult::success(null, $categoryId);
	}

	/**
	 * Mark the category as created at WP
	 * 
	 * The category is removed from the Pending or Failed list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function markCategoryAsCreated($categoryId, $externalId) : ListOperationResult
	{
		$res = $this->removeCategoryFromPending($externalId);
		if(!$res) {
			YunikLogger::error("Error removing the category {$categoryId} to Pending list.");
			return ListOperationResult::error($categoryId, $externalId);
		}

		$res = $this->removeCategoryFromFailed($externalId);
		if(!$res) {
			YunikLogger::error("Error adding the category {$categoryId} to Failed list.");
			return ListOperationResult::error($categoryId, $externalId);
		}

		return ListOperationResult::success($categoryId, $externalId);
	}

	/**
	 * Mark the category as Failed at WP
	 * 
	 * The category is removed from the Pending list
	 * and added to Failed list
	 * @return ListOperationResult the result of the operation
	 */ 
	public function markCategoryAsFailed($externalId) : ListOperationResult
	{
		$isPending = $this->categoryIsPending($externalId);
		if($isPending) {
			$this->removeCategoryFromPending($externalId);
			YunikLogger::debug("Removed the category {$externalId} from the Pending list.");
		}
		$this->addCategoryToFailed($externalId);

		return ListOperationResult::success(null, $externalId);
	}

	/**
	 * Add a category to a specific list
	 * @return bool|int false if failed, the id generated if success
	 */ 
	protected function addCategoryToList(WpCategoryDto $category, $list)
	{
		$pdo = new LocalPdo();
		$categoryId = $category->getId();
		$name = $category->getName();
		$parent = $category->getParent();

		$stmt = $pdo->prepare(self::QUERY_INSERT_LIST);
		$stmt->bindParam(':categoryId', $categoryId);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':list', $list);
		$stmt->bindParam(':parent', $parent);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		
		$pdo = null;
		if(!$res) {
			return false;
		}

		return $id;
	}
}
<?php

namespace Yunik;

use Yunik\BaseException,
	Yunik\Interfaces\CacheProviderInterface,
	Yunik\Cache\YunikCacheProvider,
	Yunik\Sync\SyncManager,
	Yunik\Sync\Sync,
	Yunik\Logger\YunikLogger,
	Yunik\Db\LocalPdo,
	Yunik\Interfaces\AppInterface,
	Yunik\Wp\WpServiceAbstract;




/**
 * Abstract application to run the Sync plugin with diferent implementations
 *
 * Each implementation is responsible to fetch and handle the data by their own
 */
abstract class AbstractApp implements AppInterface {

	const LAST_PAGE_KEY = 'rcsoft_last';

    const PRODUCT_ID_MAP_PREFIX = 'rcsoft_id_map';

    const DEBUG_FILENAME = 'debug.log';

    const REST_NAMESPACE= 'rcsoft/v1';

    const PREFIX = 'rcsoft_';

    const CAT_MAP = 'rcs_map_';

    const COUNTER_MODIFIED = 'modified_counter';

    const LAST_MODIFIED_KEY = 'last_modified';

    const SETTINGS_PREFIX = 'yunik_settings_';

    const QUERY_INSERT_HISTORY = 'INSERT INTO rcsoft_history (type, duration) VALUES(:type, :duration)';

    const QUERY_SELECT_HISTORY = 'SELECT id, type, duration, created FROM rcsoft_history ORDER BY ID DESC LIMIT 250';

	public $sync;

    public $cache;

    /**
     * Plugin data from get_plugin_data()
     * @var [type]
     */
    public $pluginData;

    /**
     * Prefix permits to run more than one version of this plugin in the same WP installation
     * Used to define slugs, database keys and others options
     * @var string
     */
    protected $prefix;

    /**
     * Name used for this plugin
     * @var string
     */
	protected $name;

	protected $webServer;

	public $syncro;

	/**
	 * Default settings defined when the plugin is activated
	 */
	const SETTINGS_DEFAULTS = [
		'categories_map_enabled' => false,
		'per_queue' => 100,
		'emails' => 'webdesign@yunik.us',
		'basePath' => 'Macovex',
		'basePort' => 6069,
		'stockUrl' => 'StockAnalises/api/Existencias/ObterExistencias',
		'priceUrl' => 'Comercial/api/Precario/ObterLista?TabelaPrecos=01&Moeda=EUR',
		'parametricUrl' => 'Produtos/api/ProdutoParametrico/ObterDados',
		'promoUrl' => 'MacoWBS/WebService1.asmx/obterPromos',
		'promosUrl' => 'MacoWBS/WebService1.asmx/obterPromosHeaders',
		'categoriesUrl' => 'MacoWBS/WebService1.asmx/obterCategorias',
		'baseUrl' => 'http://wbs.macovex.pt',
		'throwNotMapped' => false,
		'debug' => false,
		'rewrite' => '/'
	];

    public function __construct($pluginData = null, $prefix = 'yunik')
    {
    	$this->pluginData = $pluginData;
    	$this->prefix = strtolower($prefix) . '_';
    	$this->name = $prefix;
    	$this->webServer = 'nginx';
    	$this->syncro = new Sync();
    }

   	abstract public function parseItems(array &$items) : void;

    abstract public function importNewProducts() : bool;

    abstract public function createProduct($item);

    /**
     * Return the prefix for this application
     *
     * Prefix is used to maintain two or more versions of the same plugin
     * executing in the same WP installation
     * @return string the prefix
     */
    public function getPrefix() : string
    {
    	return $this->prefix;
    }

    /**
     * Return the name for this application
     * @return string the name
     */
    public function getName() : string
    {
    	return $this->name;
    }

    /**
     * Return the cache component
     * @return CacheProviderInterface the cache component
     */
    public function getCache() : CacheProviderInterface
    {
    	return $this->cache;
    }

    /**
     * Register all required hooks
     * @return [type] [description]
     */
    public function registerHooks() : void
    {
    	// Register actions
    	add_action('wp_dashboard_setup', array($this, 'registerDashboardWidget'));
		add_action('admin_menu', array($this, 'addSettingsPage'));
		add_action('admin_bar_menu', array($this, 'registerAdminBar'), 100);
		add_action('rest_api_init', array($this, 'registerApiHooks'));
		add_action('init', array($this, 'createMetaboxes'));
		add_action('admin_enqueue_scripts', array($this, 'registerAdminScripts'));
		add_action('wp_enqueue_scripts', array($this, 'registerScripts'), 20);
		add_action('wp_trash_post', array($this, 'onDeletePost'));


		// Register flters
		add_filter('plugin_action_links_' . PLUGIN_LINKS_PATH, array($this, 'addPluginSettingsLink'));
		add_filter('cron_schedules', array($this, 'cronSchedules'));
		add_filter('acf/fields/taxonomy/result', array($this, 'formatTaxonomySelect'), 10, 4);

		if (!wp_next_scheduled('rcsoft_cron_hook') ) {
		    wp_schedule_event(time(), '1min', 'rcsoft_cron_hook');
		}
		add_action('rcsoft_cron_hook', array($this, 'cronExecute'));

		// Check for dirty products
		/*if (!wp_next_scheduled('rcsoft_cron_clean_hook') ) {
		    wp_schedule_event(time(), '30min', 'rcsoft_cron_clean_hook');
		}
		add_action('rcsoft_cron_clean_hook', array($this, 'cronCleanExecute'));*/

		register_activation_hook(PLUGIN_ROOT, array($this, 'setupPlugin'));
    }

    public function formatTaxonomySelect($title, $term, $field, $postId)
    {
    	// add the term's slug to each result
	    $title .= ' (';
	    if($parent =  $term->parent) {
	    	$parentTax = get_term($parent, 'product_cat');
	    	if($parentParent = $parentTax->parent) {
	    		$parentParentTax = get_term($parentParent, 'product_cat');
	    		$title .= $parentParentTax->name . ' -';
	    	}
	    	$title .= $parentTax->name . ' - ';
	    }
	    $title.=  $term->description .  ')';

	    return $title;
    }

    /**
     * Invoked when a post is deleted
     * @param  int $postId the post ID
     */
    public function onDeletePost($postId)
    {
    	$post = get_post($postId);

    	// Post should never be null as it was trashed, not deleted. Even so..
    	if($post === null) {
    		YunikLogger::error('Post with ID ' . $postId . ' not found.');
    		return;
    	}

    	if($post->post_type === 'product') {
    		$map = $this->sync->getProductMapByWoocommerce($post->ID);
    		if(!is_array($map)) {
    			return;
    		}
    		
    		$rcsoftId = $map['rcsoft'];
    		if(!is_numeric($rcsoftId)) {
    			return;
    		}
	
			$this->sync->removeProductMap($post->ID, $rcsoftId);
			YunikLogger::debug("Removed the map for product ");
		
    	}

    	// If we can't write then something is wrong
    	if(!is_writable(PLUGIN_REDIRECT_FILE)) {
    		YunikLogger::error("Failed to add a link to the redirect list. The file isn\'t writtable, located at: " . PLUGIN_REDIRECT_FILE);
    		return;
    	}

    	if($post->post_type !== 'product') {
    		return;
    	}

    	$url = get_permalink($postId);

    	$rule = 'rewrite ' . $url . ' ' . $this->cache->get(self::SETTINGS_DEFAULTS . 'rewrite');
    	$file = fopen(PLUGIN_REDIRECT_FILE, 'a');
    	fwrite($file, $rule . "\n");
    	fclose($file);
    }

    /**
     * Reload the web server configuration
     */
    public function reloadWebServer() : void
    {
    	// Ignore user aborts, otherwise PHP script will stop executing at this point
    	ignore_user_abort(true);

    	switch ($this->webServer) {
    		case 'nginx':
    			$this->reloadNginx();
    			break;

    		default:
    			throw new \Exception("Plugin only supports Nginx webserver. Detected: " . $this->webServer);
    			break;
    	}
    }

    /**
     * Reload the nginx configuration
     */
    public function reloadNginx() : void
    {
    	exec(PLUGIN_ROOT . '/includes/reload-nginx.sh');
    }

    /**
     * Invoked when the plugin is activated
     */
    public function setupPlugin() : void
    {
    	$keys = $this->setupDefaultSettings();
    	YunikLogger::debug(count($keys) > 0 ? 'Default keys: ' . print_r($keys) : 'No default keys saved');

    	if(!$this->isInstalled()) {
    		$this->installPlugin();
    	}
    }

    /**
     * Indicates if the plugin was already configured
     * @return boolean [description]
     */
    public function isInstalled() : bool
    {
    	return $this->cache->get(self::PREFIX . 'installed') ?? false;
    }


    /**
     * Configure the plugin with the default settings
     */
    public function setupDefaultSettings() : array
    {
    	// Keep track of which defaults were created
    	$keys = [];
    	foreach (self::SETTINGS_DEFAULTS as $key => $default) {
    		// None of settings should be empty
    		if(empty($this->cache->get(self::SETTINGS_PREFIX . $key))) {
    			$this->cache->set(self::SETTINGS_PREFIX . $key, $default);
    			$keys[] = $key;
    		}
    	}

    	return $keys;
    }

    /**
     * Mark the plugin as installled
     */
    public function installPlugin()
    {

    	$this->cache->set(self::PREFIX . 'installed', true);
    	YunikLogger::log('Plugin installed.');
    }

    /**
     * Indicates if the plugin should map the categories
     * @return boolean true if the categories should be mapped, false otherwise
     */
    public function hasCategoriesMapEnabled() : bool
    {
    	return true;
    }

    /**
     * Indicates if the plugin has cron jobs enabled
     * @return boolean true if the cron jobs are enabled, false otherwise
     */
    public function hasCronEnabled() : bool
    {
    	return true;
    }

    /**
     * Add a new menu to the admin top bar
     * @param  mixed $adminBar tjhe admin bar objet
     * @return mixed           the same object
     */
    public function registerAdminBar($adminBar)
    {
    	if(!current_user_can('administrator')) {
    		return;
    	}
    	
		$adminBar->add_menu(array(
	        'id' => $this->prefix . 'sync',
	        'title' => $this->name,
	        'href' => get_admin_url() . 'admin.php?page=' . $this->prefix . 'tools',
	        'meta' => array(
	            'title' => __('RCSoft Sync'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'dashboard',
	        'title' => 'RCSoft Sync',
	        'parent' => $this->prefix . 'sync',
	        'href' => get_admin_url() . 'admin.php?page=' . $this->prefix . 'tools',
	        'meta' => array(
	            'title' => __('RCSoft Sync'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'sync-queue',
	        'title' => 'Operações em Progresso',
	        'parent' => $this->prefix . 'sync',
	        'href' =>get_admin_url() . 'admin.php?page=' . $this->prefix . 'queues',
	        'meta' => array(
	            'title' => __('Operações em Progresso', 'macovex'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'sync-mail',
	        'title' => 'Emails Pendentes',
	        'parent' => $this->prefix . 'sync',
	        'href' =>get_admin_url() . 'admin.php?page=' . $this->prefix . 'queue_email',
	        'meta' => array(
	            'title' => __('Operações em Progresso', 'macovex'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'statics',
	        'title' => 'Estatísticas',
	        'parent' => $this->prefix . 'sync',
	        'href' => get_admin_url() . 'admin.php?page=' . $this->prefix . 'statics',
	        'meta' => array(
	            'title' => __('Estatísticas', 'macovex'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'debug',
	        'title' => 'Depuração',
	        'parent' => $this->prefix . 'sync',
	        'href' => get_admin_url() . 'admin.php?page=' . $this->prefix . 'debug',
	        'meta' => array(
	            'title' => __('Depuração de Problemas', 'macovex'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => $this->prefix . 'categories',
	        'title' => __('Mapeamento de Categorias', 'macovex'),
	        'parent' => $this->prefix . 'sync',
	        'href' => get_admin_url() . 'admin.php?page=' . $this->prefix .  'categories_mapping',
	        'meta' => array(
	            'title' => __('Mapeamento de Categorias'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    $adminBar->add_menu(array(
	        'id' => 'rcsoft-general-settings',
	        'title' => 'Opções',
	        'parent' => $this->prefix . 'sync',
	        'href' => get_admin_url() . 'admin.php?page=' . 'rcsoft-general-settings',
	        'meta' => array(
	            'title' => __('Opções do RCSoft'),
	            'class' => 'admin-bar__rcsoft'
	        ),
	    ));

	    return $adminBar;
    }

	/**
	 * Add more intervals to the WP Cron
	 * @param  array $schedules WP existing schedules
	 * @return array            the new array of schedules
	 */
	public function cronSchedules($schedules){

	    if(!isset($schedules["1min"])){
	        $schedules["1min"] = array(
	            'interval' => 60,
	            'display' => __('Once every 1 minute', 'rcsoft'));
	    }

	    if(!isset($schedules["5min"])){
	        $schedules["5min"] = array(
	            'interval' => 5*60,
	            'display' => __('Once every 5 minutes', 'rcsoft'));
	    }

	    if(!isset($schedules["30min"])){
	        $schedules["30min"] = array(
	            'interval' => 30*60,
	            'display' => __('Once every 30 minutes', 'rcsoft'));
	    }

	    return $schedules;
	}

	/**
	 * Clean unavailable products
	 */
	public function cronCleanExecute()
	{
		$start = microtime(true);
		// Zero the stock of products without price
		$query = [
    		'post_type' => 'product',
    		'posts_per_page' => -1,
    		'status' => 'publish'
    	];
    	$products = new \WP_Query($query);
    	
    	// Keep track of affected products
    	$cleaned = [];

    	foreach ($products->posts as $post) {
    		$product = new \WC_Product($post);
    		$price = $product->get_price();
    		$qty = $product->get_stock_quantity();

    		if((empty($price) || $price === 0) &&  (!empty($qty) || $qty > 0)) {
    			$cleaned[] = $product->get_id();
    			$product->set_stock_quantity(0);
    			$product->set_stock_status('outofstock');
    			$product->save();
    		} else if((empty($qty) || $qty == 0) && $product->get_stock_status() == 'instock') {
    			$cleaned[] = $product->get_id();
    			$product->set_stock_status('outofstock');
    			$product->save();
    		}
    	}

    	$end = microtime(true);
		$time = $this->calculateTimeElapsed($start, $end);

		$this->addCronHistory($time, 'wc_clean');

		return rest_ensure_response($cleaned);
	}

	/**
	 * Execute the CRON jobs
	 *
	 * This method is invoked by WP_Cron
	 */
	public function cronExecute()
	{

		$cronEnabled = get_field('rcsoft_cron_enabled', 'option') ?? false;
	
		if(!$cronEnabled) {
			YunikLogger::debug("Cron is disabled. Stop execution.");
			return;
		}

		$start = microtime(true);
		require_once(ABSPATH . 'wp-admin/includes/admin.php');

		wp_set_auth_cookie(1);

		if($this->cronIsExpired()) {
			$this->unlockCron();
			YunikLogger::debug("Cron was expired, unlocked");
		}
		else if($this->cronIsLocked()) {
			YunikLogger::debug("Cron is already executing. Aborting");
			// if already passex X, unlock (?)
			return;
		}

		$this->lockCron();
		$type = 'default';

		$categoriesMapEnabled = get_field('rcsoft_category_mapping', 'option') ?? false;
		$updateCategories = get_field('rcsoft_category_sync', 'option') ?? false;

		// && 
		if($this->hasCategoriesInqueue()) {
			$this->executeCategoryPendingQueue();
			$type = 'category_queue';
		} else if($this->hasItensInQueue()) {
			$this->executeProductPendingQueue();
			$type = 'product_queue';
		} else {
			if(!$categoriesMapEnabled && $updateCategories) {
				$this->updateCategories();
			}
			$this->execute();
			$type = 'fetch';
		}

		$this->updateLastModified();
		$this->unlockCron();
		$end = microtime(true);

		$time = $this->calculateTimeElapsed($start, $end);
		$this->addCronHistory($time, $type);

	}


	/**
	 * Check if the cron is already running
	 * @return bool true if cron is running, false otherwise
	 */
	public function cronIsLocked() : bool
	{
		$locked = $this->cache->get($this->prefix . 'cron_locked');
		return $locked ? $locked : false;
	}

	/**
	 * Mark the cron system as running
	 */
	protected function lockCron() : void
	{
		$this->cache->set($this->prefix . 'cron_locked', true);
		$this->cache->set($this->prefix . 'cron_locked_at', time());
		YunikLogger::debug("Locking Cron");
	}

	/**
	 * Check if a cron is locked and also expired
	 * @param  integer $expireBy the seconds of expiration
	 * @return bool            true if cron is locked and expired, false otherwise
	 */
	protected function cronIsExpired($expireBy = 20) : bool
	{
		$lockedAt = $this->cache->get($this->prefix . 'cron_locked_at');
		if(!$lockedAt) {
			return true;
		}

		return (time() - $lockedAt) > $expireBy;
	}

	/**
	 * Mark the cron system as not running
	 * @return [type] [description]
	 */
	protected function unlockCron() : void
	{
		$this->cache->set($this->prefix . 'cron_locked', false);
		YunikLogger::debug("Unlocking Cron");
	}

	/**
	 * Add a new cron history
	 * @param string $time time elapsed
	 * @param string $type the operation type, ie: fetch, queue
	 */
	protected function addCronHistory($time, $type)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_INSERT_HISTORY);
		$stmt->bindParam(':duration', $time);
		$stmt->bindParam(':type', $type);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		$pdo = null;

		if(!$res) {
			YunikLogger::error("Error creating a new history.");
			return false;
		}
	}

	/**
	 * Return a list of cron histories
	 * @return array array containing cron histories
	 */
	protected function getCronHistory()
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_HISTORY);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}

	/**
	 * Register required scripts for this plugin
	 */
	public function registerScripts() : void
	{

	}

	/**
	 * Register required scripts for this plugin only in WP-Admin
	 */
    public function registerAdminScripts()
    {
    	wp_enqueue_style('macovex_admin', PLUGINS_URL . '/resources/admin.css');
    }

    /**
     * Register the Dashbord Widget for RCSoft
     * @return [type] [description]
     */
    public function registerDashboardWidget()
	{
		wp_add_dashboard_widget('dashboard_widget', __('Macovex Sync', 'macovex'), array($this, 'renderDashboardMacovexStats'));
	}

	/**
	 * Render the Dasbhoarb Widget for RCSoft
	 */
	public function renderDashboardMacovexStats()
	{
		$counters = $this->getQueueCounters();
		$prodPending = $counters['prodPending'];
		$prodFailed = $counters['prodFailed'];
		$catPending = $counters['catPending'];
		$catFailed = $counters['catFailed'];
		$invoked = $counters['invoked'];
		$lastModified = $this->getLasModified();
		$url = menu_page_url($this->prefix . 'tools', false);

		echo <<<EOF
		<p>Report any problem to <a href="mailto:webdesign@yunik.us">webdesign@yunik.us</a></p>
		<ul class="macovex_dash__stats">
			<li class="macovex_dash__prod-pend">
				<a href="$url">
					<strong>$prodPending products</strong>
					pending queue
				</a>
			</li>
			<li class="macovex_dash__prod-fail">
				<a href="$url">
					<strong>$prodFailed products</strong>
					failed queue
				</a>
			</li>
		</ul>
		<ul class="macovex_dash__stats">
			<li class="macovex_dash__cat-pend">
				<a href="$url">
					<strong>$catPending categories</strong>
					pending queue
				</a>
			</li>
			<li class="macovex_dash__cat-fail">
				<a href="$url">
					<strong>$catFailed categories</strong>
					failed queue
				</a>
			</li>
		</ul>
		<p><i>Last execution: $lastModified<br>Invoked $invoked times</i>
EOF;
	}

	/**
	 * Return the date of the last time the crun was executed
	 * @return mixed the date]
	 */
	public function getLasModified()
	{
		$lastModified = $this->cache->get(self::LAST_MODIFIED_KEY);
		if(empty($lastModified)) {
			$lastModified = $this->updateLastModified();
		}

		return $lastModified;
	}

	/**
	 * Update the last time the cron was executed
	 */
	public function updateLastModified()
	{
		$date = date("Y-m-d H:i:s");
		$this->cache->set(self::LAST_MODIFIED_KEY, $date);
		$this->cache->incrBy(self::COUNTER_MODIFIED);
		return $date;
	}

	/**
	 * Return a array containing the counters for the queue lists
	 * @return array the counter array
	 */
	public function getQueueCounters()
    {
        return [
            'catPending' => $this->sync->countCategories(SyncManager::LIST_PENDING),
            'catFailed' => $this->sync->countCategories(SyncManager::LIST_FAILED),
            'prodPending' => $this->sync->countProducts(SyncManager::LIST_PENDING),
            'prodFailed' => $this->sync->countProducts(SyncManager::LIST_FAILED),
            'invoked' => $this->cache->get(self::COUNTER_MODIFIED, 1)
        ];
    }

    /**
     * Add links to the plugin information
     * @param array $links [description]
     * @return array the links array
     */
	public function addPluginSettingsLink($links)
	{
	   $links[] = '<a href="'. esc_url(get_admin_url() . 'admin.php?page=' . $this->prefix .  'tools') . '">Dashboard</a>';
	   $links[] = '<a href="'. esc_url(get_admin_url() . 'admin.php?page=' . $this->prefix . 'settings') . '">Settings</a>';
	   return $links;
	}

	/**
	 * Register the settings page in WP Admin Bar
	 */
    public function addSettingsPage() : void
    {
    	if(!current_user_can('administrator')) {
    		return;
    	}

    	add_menu_page(__($this->name . ' Sync', 'rcsoft'), __($this->name . ' Sync', 'rcsoft'), 'manage_options', $this->prefix . 'tools', array($this, 'renderToolsPage'));
    	add_submenu_page($this->prefix . 'tools', __('Operações em Progresso', 'rcsoft'), __('Operações em Progresso'), 'manage_options', $this->prefix . 'queues', array($this, 'renderQueuesPage'));
    	add_submenu_page($this->prefix . 'tools', __('Emails Pendentes', 'rcsoft'), __('Emails Pendentes', 'rcsoft'), 'manage_options', $this->prefix . 'queue_email', array($this, 'renderPendingEmailsPage'));
    	add_submenu_page($this->prefix . 'tools', __('Estatísticas', 'rcsoft'), __('Estatísticas', 'rcsoft'), 'manage_options', $this->prefix . 'statics', array($this, 'renderStaticsPage'));
		add_submenu_page($this->prefix . 'tools', __('Depuração', 'rcsoft'), __('Depuração', 'rcsoft'), 'manage_options', $this->prefix . 'debug', array($this, 'renderDebugPage'));
	}

	public function renderStaticsPage()
	{
		$history = $this->getCronHistory();
		$output = '<div class="wrap">' .
			'<img src="' .  PLUGINS_URL . '/includes/images/rcsoft-logotipo-retina.png" alt="rcsoft">' .
			'<table class="wp-list-table widefat fixed striped"><thead><tr><td>Number</td><td>Date</td><td>Type</td><td>Time</td></tr></thead><tbody>';

    	foreach ($history as $index => $log) {
    		$output .= '<tr><td>' . $index++ . '</td><td>' . $log['created'] . '</td><td style="width:100%;">' . $log['type'] . '</td><td>' . $log['duration'] . '</tr>';
    	}

    	$output .= '</tbody></table></div>';
    	print_r($output);
	}

	public function renderPendingEmailsPage()
	{
		$items = $this->syncro->query('order_schedule_email');
		$output = '<div class="wrap">' .
			'<img src="' .  PLUGINS_URL . '/includes/images/rcsoft-logotipo-retina.png" alt="rcsoft">' .
			'<h1>Emails Pendentes</h1>' .
			'<a href="#" class="button button-primary show_order_items mail-queue-btn__remove-all">' . __('Cancelar Todos', 'rcsoft') . '</a>' .
			'<table class="wp-list-table widefat fixed striped"><thead><tr><td>Number</td><td>Created</td><td>Until</td><td>Id</td><td></td></tr></thead><tbody>';

		foreach ($items as $index => $queue) {
			$link = '<a href="#" order-id="' . $queue['item_id'] . '" class="button button-primary show_order_items mail-queue-btn__remove">' . __('Cancelar', 'rcsoft') . '</a>';
			$output .= '<tr id="order-queue-mail__row_' . $queue['item_id'] .'" class="order-queue-mail__row"><td>' . $index++ . '</td><td>' . $queue['created'] . '</td><td style="width:100%;">' . $queue['until'] . '</td><td>' . $queue['item_id'] . '<td>' . $link . '</td>' . '</tr>';
    	}

    	$output .= '</tbody></table>';
    	print_r($output);
	}

	/**
	 * Render the tools page
	 */
	public function renderToolsPage()
	{
		if (!current_user_can('manage_options')) {
			wp_die('Insufficient permissions!');
		}

		$counters = $this->getQueueCounters();
		$name = $this->getName();
		?>
		<div class="wrap">
		  <img src="<?php echo PLUGINS_URL . '/includes/images/rcsoft-logotipo-retina.png';?>" alt="rcsoft">
		  <form action="" method="post" name="mrs">
		    <h3>Operações <?php echo $name; ?></h3>
		    <div>
		    	<input class="button-primary" type="submit" name="sync" value="<?php _e('Sincronizar Produtos') ?>" />
			    <input class="button-primary" type="submit" name="categories" id="categories" value="<?php _e('Sincronizar Categorias') ?>" />
			    <input class="button-primary" type="submit" name="clear_queue" id="clear_queue" value="<?php _e('Limpar Todas as Listas') ?>" />
		    	<input class="button-primary" type="submit" id="run_cron" name="run_cron" value="<?php _e('Executar Cron'); ?>">
		    	<input class="button-primary" type="submit" id="run_clean_cron" name="run_clean_cron" value="<?php _e('Limpar Produtos'); ?>">
			</div>
			<div>
			    <input class="button-primary" type="submit" id="queueCategoryPending" name="queueCategoryPending" value="<?php _e('Executar Operações de Categorias (' . $counters["catPending"] . ')'); ?>">
			    <input class="button-primary" type="submit" id="queueCategoryFailed" name="queueCategoryFailed" value="<?php _e('Executar Erros de Categorias (' . $counters["catFailed"] . ')'); ?>">
			    <input class="button-primary" type="submit" id="queue" name="queue" value="<?php _e('Executar Operações de Produtos (' . $counters["prodPending"] . ')'); ?>">
			    <input class="button-primary" type="submit" id="queue_failed" name="queue_failed" value="<?php _e('Executar Erros de Produtos (' . $counters["prodFailed"] . ')'); ?>">
			</div>
			<div>
				<input class="button-primary" type="submit" id="exportProducts" name="exportProducts" value="<?php _e('Exportar Produtos'); ?>">
			</div>
		  </form>
		</div>
		<?php
		if(isset($_POST['exportProducts'])) {
			$this->exportProducts();
		}
		if(isset($_POST['clear_queue'])) {
		  	$this->clearQueues();
		}
		if(isset($_POST['run_cron'])) {
		  $this->cronExecute();
		}

		if(isset($_POST['run_clean_cron'])) {
		  $this->cronCleanExecute();
		}

		if(isset($_POST['run_queue'])) {
			$item = $this->getSync()->getProductFromQueue($_POST['queue_item']);
			$this->executeQueueItem($item);
			return;
		}

		if(isset($_POST['queue'])) {
		  	$this->executeProductPendingQueue();
		  	return;
		}

		if(isset($_POST['queueCategoryPending'])) {
		  	$this->executeCategoryPendingQueue();
		  	return;
		}

		if(isset($_POST['queue_failed'])) {
		  	$this->executeProductFailedQueue();
		  	return;
		}

		if (isset($_POST['sync'])) {
		  	$this->execute();
		  	return;
		}

		if(isset($_POST['categories'])) {
		  	$this->updateCategories();
		}
	}

	public function exportProducts()
    {
        $args = [
            'posts_per_page' => -1,
            'post_type' => 'product',
            'post_status' => 'any',
            'fields' => 'ids'
        ];
        $produtos = [];
        
        $the_query = new \WP_Query( $args );
        $i = 0;
        if($the_query->have_posts() ) :
            while($the_query->have_posts() ) : $the_query->the_post();
				$produtos[$i]['id'] = get_the_ID();
				$produtos[$i]['title'] = get_the_title();
                $produtos[$i]['sku'] = get_post_meta(get_the_ID(), '_sku', true);
                $i++;
            endwhile;
            wp_reset_postdata();
        
		endif;
        
		file_put_contents('/var/www/macovex.pt/html/produtos.json', json_encode($produtos, JSON_PRETTY_PRINT));
		echo 'Produtos Exportados: <a href="/download-produtos.php" target="_blank">Download</a>';
    }

	/**
	 * Render the queues page
	 */
	public function renderQueuesPage()
	{
		if (!current_user_can('manage_options')) {
		  wp_die('Insufficient permissions!');
		}

		$counters = $this->getQueueCounters();

		?>
		<div class="wrap">
		  <img src="<?php echo PLUGINS_URL . '/includes/images/rcsoft-logotipo-retina.png';?>" alt="rcsoft">
		  <form action="" method="post">
		    <h3>Listas de Operações em Progresso</h3>
		    <input class="button-primary" type="submit" name="list_pending" value="<?php _e('Categorias a Sincronizar (' . $counters["catPending"] . ')'); ?>">
		    <input class="button-primary" type="submit" name="list_failed" value="<?php _e('Erros de Categorias (' . $counters["catFailed"] . ')'); ?>">
		    <input class="button-primary" type="submit" name="list_product_pending" value="<?php _e('Produtos a Sincronizar (' . $counters["prodPending"] . ')'); ?>">
		    <input class="button-primary" type="submit" name="list_product_failed" value="<?php _e('Erros de Produtos (' . $counters["prodFailed"] . ')'); ?>">
		  </form>
		</div>
		<?php
		if(isset($_POST['list_pending'])) {
		  $this->outputListPending();
		  return;
		}

		if(isset($_POST['list_failed'])) {
		  $this->outputListFailed();
		  return;
		}

		if(isset($_POST['list_product_pending']) || isset($_GET['list_product_pending'])) {
		  $this->outputListProductPending();
		  return;
		}

		if(isset($_POST['list_product_failed'])) {
		  $this->outputListProductFailed();
		  return;
		}
	}

	/**
	 * Render the debug page
	 */
	public function renderDebugPage()
	{
		if (!current_user_can('manage_options')) {
	    	wp_die('Insufficient permissions!');
	  	}

	  	$this->echoDebugWindow();
	}

	/**
	 * Register the WP Rest API hooks
	 * @return [type] [description]
	 */
	public function registerApiHooks()
	{
		register_rest_route(self::REST_NAMESPACE, '/stock/', array(
			'methods' => \WP_REST_Server::READABLE,
			'callback' => array($this , 'getProductStock')
	    ));

	    register_rest_route(self::REST_NAMESPACE, '/clean/', array(
			'methods' => \WP_REST_Server::READABLE,
			'callback' => array($this , 'cronCleanExecute')
	    ));
	}

	/**
	 * Create the metaboxes using ACF
	 */
	public function createMetaboxes()
	{
		$prefix = RCSOFT_SETTINGS_PREFIX;
		$file = file_get_contents(PLUGIN_ROOT . '/outputs/categories.json');
		$opts = [];
		$decoded = json_decode($file, true);

		if(is_array($decoded) && count($decoded) > 0) {
			foreach ($decoded as $value) {
				if(isset($value['SubFam'])) {
					$opts[$value['SubFam']] = $value['DescrSubFam'] . ' - ' . $value['SubFam'];
					//if(!array_key_exists($value['Familia'], $opts)) {
					//	$opts[$value['Familia']] = $value['DescrFamilia'] . ' - ' . $value['Familia'];
					//}
				}
				// Insert the Familia anway, some products dont have SubFamilia
				if(isset($value['Familia'])) {
					$opts[$value['Familia']] = $value['DescrFamilia'] . ' - ' . $value['Familia'];
				} else if(isset($value['TipoProd'])) {
					$opts[$value['TipoProd']] = $value['DescrTipo'] . ' - ' . $value['TipoProd'];
				}
			}
		}

		// Sort the categories
		asort($opts);

		acf_add_options_page(array(
			'page_title' 	=> 'RCsoft Categories Mapping',
			'menu_title'	=> 'Categories Mapping',
			'menu_slug' 	=> 'rcsoft_categories_mapping',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'parent_slug' => $this->prefix . 'tools'
		));

		acf_add_local_field_group(array(
			'key' => 'categories_mapping_group',
			'title' => 'Categories',
			'fields' => array(
				array(
					'key' => 'categories_mapping',
					'label' => 'Categories Mapping',
					'name' => 'categories_mapping',
					'type' => 'repeater',
					'instructions' => 'Choose at least one category from RCSoft to sync with a category from Woocommerce.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						array (
			               'key' => 'rcsoft_cat',
			               'label' => 'RCSoft',
			               'name' => 'rcsoft_cat',
			               'type' => 'select',
			               'choices' => $opts,
			               'multiple' => true,
			               'ui' => true
			            ),
			            array (
			               'key' => 'woo_cat',
			               'label' => 'Woocommerce',
			               'name' => 'woo_cat',
			               'type' => 'taxonomy',
						   'field_type' => 'select',
			               'taxonomy' => 'product_cat',
						   'multiple' => 0,
			               'add_term' => 0
			            ),
					),
				),
			),
			'location' => array (
			    array (
			    	array (
			        	'param' => 'options_page',
			        	'operator' => '==',
			        	'value' => 'rcsoft_categories_mapping',
			      	)
			    )
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	/**
	 * Clear all queue lists for products and categories
	 */
	public function clearQueues() : void
	{
		$this->sync->clearProductLists();
		$this->sync->clearCategoryLists();
		$this->unlockCron();

		$headers = ['Action', 'Result'];
		$messages = [
        	['Queue lists removed', 'Product Pending, Product Failed, Category Pending and Category Failed']
        ];

        echo $this->echoResultTable($headers, $messages);
	}

	/**
	 * Check if there are itens in the pending queue of products
	 * @return boolean true if queue is not empty, false otherwise
	 */
	public function hasItensInQueue() : bool
	{
		return $this->sync->countProducts(SyncManager::LIST_PENDING) > 0;
	}

	/**
	 * Check if the are itens in the pending queue of categories
	 * @return boolean true if the queue is not empty, false otherwies
	 */
	public function hasCategoriesInqueue() : bool
	{
		return $this->sync->countCategories(SyncManager::LIST_PENDING) > 0
			|| $this->sync->countCategories(SyncManager::LIST_FAILED) > 0;
	}

	/**
     * Return all existing products from Woocommerce
     * @return array the products array
     */
    protected function getWoocommerceProductsIds() : array
    {
    	$results = [];
    	$query = [
    		'post_type' => 'product',
    		'posts_per_page' => -1,
    		'fields' => 'ids',
    		'status' => 'publish'
    	];
    	$products = new \WP_Query($query);
    	return $products->posts;
    }

    /**
     * Output the debug content
     */
    public function echoDebugWindow() : void
    {
    	$fileName = WP_CONTENT_DIR . '/' . self::DEBUG_FILENAME;
    	if(!file_exists($fileName)) {
    		throw new BaseException("By this point, the debug file should be available. Probably a permission issue.");
    	}
    	$file = file_get_contents($fileName);
    	$logs = explode("\n", $file);

    	$output = '<table class="wp-list-table widefat fixed striped"><thead><tr><td>Number</td><td>Log</td></tr></thead><tbody>';

    	foreach ($logs as $index => $log) {
    		$output .= '<tr><td>' . $index++ . '</td><td style="width:100%;">' . $log . '</td></tr>';
    	}

    	$output .= '</tbody></table>';
    	print_r($output);
    }

    /**
     * Helper method to echo a list of result with a table
     * @param  array  $headers  the table headers, ie: array('Id', 'ProductId', 'Sku')
     * @param  array  $messages the table tbody, ie: array(array(1, 2, 'sku_1'), array(2, 3, 'sku_2'))
     * @return [type]           [description]
     */
    protected function echoResultTable(array $headers, array $messages)
    {
    	$output = '<table class="wp-list-table widefat fixed striped"><thead><tr>';
    	foreach ($headers as $header) {
    		$output .= '<td>' . $header . '</td>';
    	}

    	$output .= '</tr></thead><tbody>';

    	foreach ($messages as $message) {
    		$output .= '<tr>';
    		foreach ($message as $entry) {
    			if(!is_string($entry)){
    				$entry = $this->parseValueAsString($entry);
    			}
    			$output .= "<td>$entry</td>";
    		}
    		$output .= '</tr>';
    	}

    	$output .= '</tr></tbody></table>';
    	return $output;
    }

    /**
     * Helper method to format values into string
     * @param  mixed $value the value
     * @return string        a readable version of the value
     */
    private function parseValueAsString($value) : string
    {
    	if(is_array($value)) {
    		return implode(', ', $value);
    	}

    	return print_r($value, true);

    }

    /**
     * Calculate the time elapsed from a two periods
     *
     * Start end End parameters can be obtained with microtime(true)
     * and to return as float
     * @source https://stackoverflow.com/questions/7850259/calculate-elapsed-time-in-php
     * @param  float  $start the start
     * @param  float  $end   the end
     * @return string        the time in hh:mm:ss
     */
    protected function calculateTimeElapsed(float $start, float $end) : string
    {
    	$timeDiff = $end - $start;
    	$h = floor($timeDiff / 3600);
		$timeDiff -= $h * 3600;
		$m = floor($timeDiff / 60);
		$timeDiff -= $m * 60;
		return $h.':'.sprintf('%02d', $m).':'.sprintf('%02d', $timeDiff);
    }

    /**
     * Helper method to echo the list of categories in the pending queue
     */
    public function outputListPending() : void
    {
    	$skip = $_GET['skip'] ?? 0;
    	$take = $_GET['take'] ?? 100;

	    $list = $this->sync->getPendingCategories($skip, $take);
	    echo '<h2>' . __('Pending Categories', 'rcsoft') . '</h2>';
	    echo $this->echoList($list);
	}

	/**
	 * Helper method to echo the list of categories in the failed queue
	 */
	public function outputListFailed() : void
	{
	    $list = $this->sync->getPendingCategories(0, 100);
	    echo '<h2>' . __('Failed Categories', 'rcsoft') . '</h2>';
	    echo $this->echoList($list);
	}

	/**
	 * Helper method to build the table for categories in the pending and failed queue
	 * @param  array $list queue list
	 * @return string       the table
	 */
	protected function echoList($list) : string
	{
	    ob_start();
	    $take = $_GET['take'] ?? 100;

	    $skip = isset($_GET['skip']) ? $_GET['skip'] + $take : $take;
	    ?>
	        <table class="wp-list-table widefat fixed striped">
	            <thead>
	                <tr>
	                    <th>Id</th>
	                    <th>Category Id</th>
	                    <th>Name</th>
	                    <th>Created</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            if(count($list) == 0) {?>
	                <tr>
	                    <td colspan="4" align="center">
	                        <?php echo __('There aren\'t any categories to show based on the actual filters.', 'rcsoft'); ?>
	                    </td>
	                </tr>
	            <?php
	            }
	            foreach ($list as $item) {
	            ?>
	                <tr>
	                    <td><?php echo $item['id']; ?></td>
	                    <td><?php echo $item['categoryId']; ?></td>
	                    <td><?php echo $item['name']; ?></td>
	                    <td><?php echo $item['created']; ?></td>
	                </tr>
	            <?php
	            }
	            ?>
	            </tbody>
	            <tfoot>
	            	<tr>
	            		<td colspan="5"><a href="<?php get_admin_url() . 'admin.php?page=' . $this->prefix . 'queues&take=' . $take . '&skip=' . $skip; ?>">Ver Mais</a></td>
	            	</tr>
	            </tfoot>
	        </table>
	        <?php
	    return ob_get_clean();
	}

	/**
	 * Helper method to echo the list of products in the pending queue
	 */
	public function outputListProductPending() : void
	{
		$take = $_GET['take'] ?? 100;
    	$skip = isset($_GET['skip']) ? $_GET['skip'] : $take;
		$list = $this->sync->getPendingProducts($skip, $take);
	    echo '<h2>' . __('Pending Products', 'rcsoft') . '</h2>';
	    echo $this->echoListProducts($list);
	}

	/**
	 * Helper method to echo the list of products in the failed queue
	 */
	public function outputListProductFailed() : void
	{
	    $list = $this->sync->getFailedProducts(0, 100);
	    echo '<h2>' . __('Failed Products', 'rcsoft') . '</h2>';
	    echo $this->echoListProducts($list);
	}

	/**
	 * Helper method to build the table for products in pending and failed queue
	 * @param  array $list queue list of items
	 * @return string       the table
	 */
	protected function echoListProducts($list, $skip = 0, $take = 100) : string
	{
	    ob_start();

	    ?>
	        <table class="wp-list-table widefat fixed striped">
	            <thead>
	                <tr>
	                    <th>Queue Id</th>
	                    <th>Product Id</th>
	                    <th>SKU</th>
	                    <th>Stock</th>
	                    <th>Price</th>
	                    <th>Price Promo</th>
	                    <th>Promo Start</th>
	                    <th>Promo End</th>
	                    <th>Category</th>
	                    <th>Created</th>
	                    <th>Step Value</th>
	                    <th>Brand</th>
	                    <th>Weight</th>
	                    <th></th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            if(count($list) == 0) {?>
	                <tr>
	                    <td colspan="9" align="center">
	                        <?php echo __('There aren\'t any products to show based on the actual filters.', 'rcsoft'); ?>
	                    </td>
	                </tr>
	            <?php
	            }
	            foreach ($list as $item) {
	            ?>
	                <tr>
	                	<?php
	                	$fields = ['id', 'externalId', 'sku', 'stock', 'price', 'promo', 'promoStart', 'promoEnd', 'category', 'created', 'stepValue', 'brand', 'weight'];

	                	foreach ($fields as $field) {
	                		$fieldVal = isset($item[$field]) ? $item[$field] : '';
	                		if(!is_string($fieldVal)) {
	                			$fieldVal = $this->parseValueAsString($fieldVal);
	                		}
	                		echo "<td>" . $fieldVal . '</td>';
	                	}
	                	?>
	                    <td>
	                        <form action="" method="POST">
	                            <input type="hidden" name="queue_item" value="<?php echo $item['id']; ?>">
	                            <input type="submit" name="run_queue" value="<?php echo __('Run'); ?>" class="button-primary" >
	                        </form>
	                    </td>
	                </tr>
	            <?php
	            }
	            ?>
	            </tbody>
	            <tfoot>
	            	<tr>
	            		<td colspan="13"><a href="<?php echo get_admin_url() . 'admin.php?page=' . $this->prefix . 'queues&list_product_pending=1&take=' . $take . '&skip=' . $skip; ?>">Ver Mais</a></td>
	            	</tr>
	            </tfoot>
	        </table>
	        <?php
	    return ob_get_clean();
	}

	/**
	 * Execute jobs in the pending queue list of categories
	 * @return [type] [description]
	 */
	public function executeCategoryPendingQueue()
    {
        $idsProcessed = [];
        $idsRcsoft = [];

        $take = $this->cache->get(self::SETTINGS_PREFIX . 'per_queue');
        if(!$take) {
            YunikLogger::error("Error retreving the per_queue option for paging. Continuing with default 20 per execution of queues.");
            $take = 20;
        }

        $list = $this->sync->getPendingCategories(0, $take);

        foreach ($list as $item) {
            $this->executeQueueCategoryItem($item);
            $idsProcessed[] = $item['id'];
            $idsRcsoft[] = $item['categoryId'];
        }

        $this->removeCategoriesFromPending($idsRcsoft);

        $this->executeQueue($list, $idsProcessed, 'Categories');
    }

    /**
     * Execute jobs in the pending queue list of products
     * @return [type] [description]
     */
    public function executeProductPendingQueue()
    {

        $idsProcessed = [];
        $take = $this->cache->get(self::SETTINGS_PREFIX . 'per_queue');
        if(is_null($take) || empty($take)) {
            YunikLogger::error("Error retreving the per_queue option for paging. Continuing with default 20 per execution of queues.");
            $take = 200;
        }

        $list = $this->sync->getPendingProducts(0, $take);
        foreach ($list as $item) {
            $this->executeQueueItem($item);
            $idsProcessed[] = $item['externalId'];
        }

        $this->executeQueue($list, $idsProcessed, 'Products');
    }

    /**
     * Execute jobs in the failed queue of jobs
     */
    public function executeProductFailedQueue()
    {
        $idsProcessed = [];
        $take = $this->cache->get(self::SETTINGS_PREFIX . 'per_queue');
        if(is_null($take) || empty($take)) {
            YunikLogger::error("Error retreving the per_queue option for paging. Continuing with default 20 per execution of queues.");
            $take = 20;
        }

        $list = $this->sync->getPendingProducts(0, $take);

        foreach ($list as $item) {
            $externalId = $item['sku'];
            $product = new WpProductDto(null, $externalId, $item['stock'], $item['price'], $item['promo'], $item['promoStart'], $item['promoEnd']);;
            $this->sync->addProductToPending($product);
            $this->sync->removeProductFromFailed($externalId);
            $idsProcessed[] = $externalId;
        }

        $this->executeQueue($list, $idsProcessed, 'Products');

        return;
    }

	/**
	 * Execute jobs in the pending queue
	 */
	public function executeQueue($list, $idsProcessed, $name)
	{
        $counter = count($idsProcessed);
        $idsStr = implode(', ', $idsProcessed);

		$headers = ['Action', 'Result'];
        $messages = [
            ['Queue items processed', $counter],
            ['Queue item ' . $name . ' Ids processed', $idsProcessed]
        ];

        echo $this->echoResultTable($headers, $messages);
	}

	/**
	 * Execute a single job in the queue category
	 * @param  array $item the queue item
	 */
    public function executeQueueCategoryItem($item)
    {
        $catId = $item['categoryId'];
        $exists = $this->sync->categoryIsPending($catId);

        if(!$exists) {
            YunikLogger::error("Queue category $catId can't be executed. Not found in pending, continuing");
            return;
        }

        // Sanitize
        //$slug = 'rcsoft_' . strtolower(str_replace(' ', '_', $item['name']));
        
        // Create without a parent. Most likely don't have the ID yet
        $category = [
            'taxonomy' => 'product_cat',
            'cat_name' => $item['name']
           // 'category_nicename' => $slug
        ];

        if(isset($item['parent']) && !empty($item['parent'])) {
            $category['category_parent'] = $this->cache->get(self::CAT_MAP . $item['parent']);
        }

        $wpCatId = wp_insert_category($category, true);

        if($wpCatId instanceof \WP_Error) {
            $errorCode = $wpCatId->get_error_code();
            if($errorCode === 'term_exists') {
                return;
            }

            throw new BaseException("Unexpected error: $errorCode for RCSoft category $catId");
        }

        if(empty($wpCatId)) {
            throw new BaseException("Inserted category didnt returned a valid Id. RCSoft ID $catId");
        }

        $this->cache->set(self::CAT_MAP . $catId, $wpCatId);
    }

    /**
     * Remove jobs from the pending queue list of categories
     * @param  array $ids the ids of jobs to be removed
     */
    protected function removeCategoriesFromPending($ids)
    {
        foreach ($ids as $id) {
            $wpCatId = $this->cache->get(self::CAT_MAP . $id);

            if(empty($wpCatId)) {
                //$this->removeCategoriesFromPending($id);
                $this->sync->removeCategoryFromPending($id);
                $throw = $this->cache->get(self::SETTINGS_PREFIX . 'throwNotMapped');
                if($throw) {
                    //throw new BaseException("Category $id not mapped. Was invoked in the pending queue.");
                }
                continue;
            }

            $item = $this->sync->getPendingCategory($id);

            /*if(isset($item['parent']) && $item['parent'] !== null) {
                $category['category_parent'] = $item['parent'];
                $parentId = $this->cache->get(self::CAT_MAP . $item['parent']);

                wp_update_term($wpCatId, 'product_cat', array(
                    'category_parent' => $parentId
                ));

            }*/

            $res = $this->sync->removeCategoryFromPending($id);

            if(!$res->isOk()) {
                YunikLogger::error("Error removing a product from pending with id $id");
            }
        }

    }

    /**
	 * Execute a single job in the queue list of products
	 * @param  array $item the job item
	 */
	public function executeQueueItem($item)
	{

	    $externalId = $item["externalId"];
      	$id = null;

      	$updateCategories = get_field('rcsoft_category_sync', 'option') ?? false;
      	$existsWp = $this->sync->productMapExistsByRcsoft($externalId);
	    $exists = $existsWp && $this->sync->productExists($externalId);

		if(!$existsWp) {
			if($this->importNewProducts()) {
	      	$id = $this->createProduct($item);
	        $this->sync->addProductMap($id, $externalId);
	        YunikLogger::debug("Created a new product with Id $id for SKU $externalId");
	      } else {
	      	YunikLogger::error("Product SKU $externalId and ID $id isn't mapped. Continuing");
	        $this->sync->removeProductFromPending($externalId);
	        return;
				}
			} else {
				$map = $this->sync->getProductMapByRcsoft($externalId);
				$id = $map['woocommerce'];
			}

	    if(!$exists) {
	    	if($existsWp && $this->sync->productIsPending($id)) {
	    		$this->sync->removeProductFromPending($id);
	    	}
	    }

	    update_post_meta($id, '_stock', $item["stock"] );
	    $stockStatus = $item['stock'] == 0 ? 'outofstock' : 'instock';
	    update_post_meta($id, '_stock_status', $stockStatus);
	    update_post_meta($id, '_manage_stock', 'yes');
		
	    if (isset($item["promo"])) {
			/*
			 &&
				isset($item['promoStart']) &&
				isset($item['promoEnd'])*/
        	update_post_meta($id, '_price', $item["promo"]);
          	update_post_meta($id, '_sale_price', $item["promo"]);
			$price = $item['promo'];
	        $promoCounter++;
	    } else {
			update_post_meta($id, '_sale_price', '');
			update_post_meta($id, '_sale_price_dates_from', '');
			update_post_meta($id, '_sale_price_dates_to', '');
	        update_post_meta($id, '_price', $item["price"]);
			update_post_meta($id, '_regular_price', $item["price"]);
			$price = $item['price'];
		}

		if(floatval($price) <= 0) {
			wp_update_post([
				'post_type' => 'product',
				'ID' => $id,
				'post_status' => 'draft'
			]);
			YunikLogger::error("O produto tem o preço a zero. " . $id);
		} else if(get_post_status($id) == 'draft'){
			wp_update_post([
				'post_type' => 'product',
				'ID' => $id,
				'post_status' => 'publish'
			]);
		}
		
		if(isset($item['promoStart'])) {
			update_post_meta($id, '_sale_price_dates_from', $item["promoStart"]);
		}
		if(isset($item['promoEnd'])) {
			update_post_meta($id, '_sale_price_dates_to', $item["promoEnd"]);
		}
		if(isset($item['promoCat'])) {
			wp_set_post_terms($id, $item['promoCat'], 'promocoes');
		}

	    if(isset($item['stepValue'])) {
	    	update_post_meta($id, '_wpbo_override', 'on');
	    	update_post_meta($id, '_wpbo_minimum', $item['stepValue']);
	    	update_post_meta($id, '_wpbo_step', $item['stepValue']);
	    	update_post_meta($id, '_wpbo_maximum', $item['stock']);
	    }

	    if(isset($item['weight']) && !empty($item['weight'])) {
	    	update_post_meta($id, '_weight', $item['weight']);
	    }

	    //$categoriesMapEnabled = get_option('mapCategories');
	    $categoriesMapEnabled = get_field('rcsoft_category_mapping', 'option') ?? false;

	    $catSet = false; // flag to know if categiries were set
	    $rcSoftTermId = $item['category'];
        $wpTermId = $this->cache->get(self::CAT_MAP . $rcSoftTermId);

		/*
	    if($categoriesMapEnabled){
			// Existing categories map
			$mapped = [];

			if(have_rows('categories_mapping', 'option')) {
				while(have_rows('categories_mapping', 'option')) {
					the_row();
					$wooId = get_sub_field('woo_cat');
					//$term = get_term_by('id', $wooId, 'product_cat');
					$rcsoftIds = get_sub_field('rcsoft_cat');
					if(is_array($rcsoftIds)) {
						foreach ($rcsoftIds as $rcsoftId) {
							$mapped[$rcsoftId] = $wooId;
							$catSet = true;
						}
					}
				}

				if(array_key_exists($rcSoftTermId, $mapped)) {
					$term = get_term($mapped[$rcSoftTermId], 'product_cat');
					wp_set_post_terms($id, [$term->term_id], 'product_cat');
					YunikLogger::debug("Category from RCSoft $rcSoftTermId mapped to " . $term->term_id);
				} else {
			      	$term = get_term($wpTermId, 'product_cat');
			      	wp_set_post_terms($id, $term->term_id, 'product_cat');
			    }
			}

      	} else if(isset($item['category']) && !$catSet && $updateCategories) {
          
			if(empty($wpTermId)) {
		      	// check if its mapped, string is slug
		        $term = get_term($rcSoftTermId, 'product_cat');
		        if($term) {
		        	wp_set_post_terms($id, $term->name, 'product_cat');
		        	YunikLogger::debug($term->term_id . " is mapped.");
		        }
		        // ADD TO FAILED?!
		      	//return;
		    } else {
		      	$term = get_term($wpTermId, 'product_cat');
		      	wp_set_post_terms($id, $term->term_id, 'product_cat');
		    }

      	} else {
        	YunikLogger::debug("Found a product in queue without category or update categories is not enabled in settings. Product ID $id");
		  }*/
		if(isset($item['category'])) {
			wp_set_post_terms($id, $item['category'], 'product_cat');
		} else {
			YunikLogger::debug("Found a product in queue without category or update categories is not enabled in settings. Product ID $id");
		}

		if(isset($item['sort']) && $item['sort'] != null) {
			wp_update_post([
				'ID' => $id,
				'menu_order' => $item['sort']
			]);
		}

      	if(isset($item['brand'])) {
      		$brand = $item['brand'];
      		$res = wp_set_object_terms($id, $brand, 'marca');
      		if(!is_array($res)) {
      			YunikLogger::error("Error while set the taxonomy for SKU $sku and WP ID $wooId. Returned: " . print_r($res, true));
      		}
      	}

	    $res = $this->sync->removeProductFromPending($externalId);

	    if(!$res->isOk()) {
	        YunikLogger::error("Error removing a product from pending with id $id");
	    }
	}

	/**
	 * Return a map of products of Woocommerce and RCSoft API
	 * @param  array $productsWoo the woocommerces array
	 * @return array              the map of the products
	 */
	public function getProductsMap($productsWoo)
	{
		$productsMap = [];
		// Add existing products from Woocommerce already added from RCSoft
    	foreach ($productsWoo as $id) {
    		$productWoo = wc_get_product($id);
    		if(!$productWoo) {
    			YunikLogger::error("Product was returned from Woocommerce but cant get it from wc_get_product.");
    			continue;
    		}
    		$rcsoftId = $productWoo->get_sku();

    		if(empty($rcsoftId)) {
    			continue;
    		}

    		$productsMap[$rcsoftId] = $id;
    	}

    	return $productsMap;
	}
}
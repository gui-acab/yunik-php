<?php

declare(strict_types=1);

namespace Yunik;


/**
 * @source https://gist.github.com/sallar/5257396
 */
class Console {

	static $foregroundColors = [
		'bold' => '1'
	];

	static $backgroundColors = [
		'black' => '40',
		'red' => '41',
		'green' => '42',
		'yellow' => '43',
		'blue' => '44',
		'magenta' => '45',
		'cyan' => '46',
		'lightGray' => '47'
	];

	static $options = [
		'underline' => '4',
		'blink' => '5',
		'reverse' => '7',
		'hidden' => '8'
	];

	static $EOF = "\n";

	public static function log($message, $color = 'normal', $newLine = true, $backgroundColor = null)
	{
		$message = $newLine ? $message . self::$EOF : $message;

		print_r($message);

	}

	public static function black($message)
	{
		self::log($message, self::$backgroundColors['black']);
	}
}
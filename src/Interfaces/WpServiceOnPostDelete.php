<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




interface WpServiceOnPostDelete {
	
	public function onDelete(int $postId);
}
<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




interface WpServiceOnPostTrash {

	public function onTrash(int $postId);
}
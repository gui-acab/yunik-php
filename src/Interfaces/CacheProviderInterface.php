<?php

namespace Yunik\Interfaces;




/**
 * Chache Provider Interface
 */
interface CacheProviderInterface {
	
	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	public function get(string $key, $default = null);

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	public function set(string $key, $value) : void;

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	public function incrBy($key, $by = 1) : int;
}
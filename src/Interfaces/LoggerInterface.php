<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




/**
 * Logger interface
 */
interface LoggerInterface {

	/**
	 * Log a debug message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	static function debug($message) : void;

	/**
	 * Log a regular message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	static function log($message) : void;

	/**
	 * Log a error message
	 * @param  string          $message the message
	 * @param  \Exception|null $ex      a exception
	 */
	static function error($message) : void;
}
<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




interface WpServiceInterface {

	public function getName() : string;

	public function getRestBasePath() : string;

	public function registerScripts() : void;

	public function registerAdminScripts() : void;

    public function registerApiHooks() : void;
}
<?php

namespace Yunik\Interfaces;

use Yunik\Interfaces\CacheProviderInterface;




interface AppInterface {

	public function parseItems(array &$items) : void;

	public function getCache() : CacheProviderInterface;

	public function getQueueCounters();
}
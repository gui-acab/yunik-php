<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




interface WpCustomizeRegister {

	public function registerCustomize(\WP_Customize_Manager $customize) : void;
}
<?php

namespace Yunik\Domain;




class ListOperationResult {

	private $externalId;

	private $newCategoryId;

	private $error;

	protected $queueItemId;
	
	public function __construct($externalId, $newCategoryId, $error = false, $queueItemId = null)
	{
		$this->externalId = $externalId;
		$this->newCategoryId = $newCategoryId;
		$this->error = $error;
		$this->queueItemId = $queueItemId;
	}

	/**
	 * Create a success result
	 */ 
	static function success($externalId, $newCategoryId, $queueItemId = null)
	{
		return new self($externalId, $newCategoryId, false, $queueItemId);
	}

	/**
	 * Create a error result
	 */ 
	static function error($externalId, $newCategoryId, $queueItemId = null)
	{
		return new self($externalId, $newCategoryId, true, $queueItemId);
	}

	/**
	 * Return the external category ID from rcsoft
	 */ 
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * Return the new category ID created at WP
	 */ 
	public function getNewCategoryId()
	{
		return $this->newCategoryId;
	}

	/**
	 * Return the new item queue ID created
	 */
	public function getQueueItemId()
	{
		return $this->queueItemId;
	}

	/**
	 * Indicates if any error ocurred
	 */ 
	public function isOk()
	{
		return !$this->error;
	}

	public function __toString()
	{
		$external = $this->externalId;
		$created = $this->newCategoryId;

		return 'External category {$external} created with the new id {$created}';
	}
}
<?php

namespace Yunik\Domain;




class WpProductDto {

	protected $id;

	protected $sku;

	protected $stock;

	protected $price;

	protected $pricePromo;

	protected $promoStart;

	protected $promoEnd;

	protected $category;

	protected $title;

	protected $content;

	protected $stepValue;

	protected $brand;

	protected $weight;

	protected $promoCat;

	protected $order;

	public function __construct($productId = null, $sku = null, $stock = null, $price = null, $pricePromo = null,
		$promoStart = null, $promoEnd = null, $category = null, $stepValue = null, $brand = null, $weight = null, $promoCat = null, $order = null)
	{
		$this->id = $productId;
		$this->sku = $sku;
		$this->stock = $stock;
		$this->price = $price;
		$this->pricePromo = $pricePromo;
		$this->promoStart = $promoStart;
		$this->promoEnd = $promoEnd;
		$this->category = $category;
		$this->stepValue = $stepValue;
		$this->brand = $brand;
		$this->weight = $weight;
		$this->promoCat = $promoCat;
		$this->order = $order;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getSku()
	{
		return $this->sku;
	}

	public function getStock()
	{
		return $this->stock;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getPricePromo()
	{
		return $this->pricePromo;
	}

	public function getPromoStart()
	{
		return $this->promoStart;
	}

	public function getPromoEnd()
	{
		return $this->promoEnd;
	}

	public function getCategory()
	{
		return $this->category;
	}

	public function setCategory($id) : void
	{
		$this->category = $id;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle(string $title) : void
	{
		$this->title = $title;;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setContent(string $content) : void
	{
		$this->content = $content;
	}

	public function getStepValue()
	{
		return $this->stepValue;
	}

	public function setStepValue($stepValue) : void
	{
		$this->stepValue = $stepValue;
	}

	public function getBrand()
	{
		return $this->brand;
	}

	public function setBrand(string $brand)
	{
		$this->brand = $brand;
	}

	public function getWeight()
	{
		return $this->weight;
	}

	public function setWeight($value)
	{
		$this->weight = $value;
	}

	public function getPromoCat()
	{
		return $this->promoCat;
	}

	public function setPromoCat($promoCat)
	{
		$this->promoCat = $promoCat;
	}

	public function getOrder()
	{
		return $this->order;
	}

	public function setOrder($order)
	{
		$this->order = $order;
	}
}
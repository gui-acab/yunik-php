<?php

declare(strict_types=1);

namespace Yunik\Wp;




class WpServiceConfig {

	protected $name;

	protected $pluralName;

	protected $postType;

	protected $slug;

	protected $restBasePath;

	protected $restNamespace;

	protected $authorSupport = true;

	protected $support;

	protected $icon;

	public function __construct(
		string $name,
		string $pluralName,
		string $postType,
		string $slug,
		string $restBasePath,
		?string $restNamespace = null,
		$support = null,
		$icon = null
	)
	{
		$this->name = $name;
		$this->pluralName = $pluralName;
		$this->postType = $postType;
		$this->slug = $slug;
		$this->restBasePath = $restBasePath;
		$this->restNamespace = $restNamespace ?: 'yunik/v1';
		$this->support = $support ?: ['title', 'content'];
		$this->icon = $icon ?: 'dashicons-palmtree';
	}

	public function getName() : string
	{
		return $this->name;
	}

	public function getPluralName() : string
	{
		return $this->pluralName;
	}

	public function getPostType() : string
	{
		return $this->postType;
	}

	public function getSlug() : string
	{
		return $this->slug;
	}

	public function getRestBasePath() : string
	{
		return $this->restBasePath;
	}

	public function getRestNamespace() : string
	{
		return $this->restNamespace;
	}

	public function setAuthorSupport(bool $enabled)
	{
		$this->authorSupport = $enabled;
	}

	public function getAuthorSupport() : bool
	{
		return $this->authorSupport;
	}

	public function support()
	{
		return $this->support;
	}

	public function getIcon()
	{
		return $this->icon;
	}

	public function setIcon(string $icon)
	{
		$this->icon = $icon;
	}
}
<?php

declare(strict_types=1);

namespace Yunik\Wp\Interfaces;




interface RegisterAdminScriptsInterface {

	public function registerScripts() : void;
}
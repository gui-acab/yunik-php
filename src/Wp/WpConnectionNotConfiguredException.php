<?php

declare(strict_types=1);

namespace Yunik\Wp;

use Yunik\BaseException;




/**
 * Throwed when the WpConnection is used but not configured
 */
class WpConnectionNotConfiguredException extends BaseException {

}
<?php

declare(strict_types=1);

namespace Yunik\Wp;

use Yunik\BaseException,
    Yunik\ValidatorException,
    Yunik\Interfaces\WpServiceInterface,
    Yunik\Interfaces\WpServiceOnPostSave,
    Yunik\Interfaces\WpServiceOnPostTrash,
    Yunik\Interfaces\WpServiceOnPostDelete,
    Yunik\Interfaces\WpCustomizeRegister;






abstract class WpServiceAbstract implements WpServiceInterface {

    protected $hooks = [];

    protected $services = [];

    protected $config;

    public abstract function registerScripts() : void;

    public abstract function registerAdminScripts() : void;

    public abstract function registerApiHooks() : void;

    public function __construct(WpServiceConfig $config)
    {
        $this->config = $config;
    }

    public function registerHooks()
    {
        $svc = $this;
        $config = $this->config;
        
        add_action('init', function() use($svc, $config) {
            $svc->registerPostType(
                $config->getPostType(),
                $config->getSlug(),
                $config->getName(),
                $config->getPluralName(),
                null,
                $config->support(),
                $config->getIcon()
            );
        });

        add_action('rest_api_init', array($this, 'registerApiHooks'));

        if($this instanceof WpServiceOnPostSave) {
            add_action('save_post', array($this, 'onSave'), 10, 3); // 3 is required
        }

        if($this instanceof WpServiceOnPostTrash) {
            add_action('trash_post', array($this, 'onTrash'));
        }

        if($this instanceof WpServiceOnPostDelete) {
            add_action('delete_post', array($this, 'onDelete'));
        }

        if($this instanceof WpCustomizeRegister) {
            add_action('customize_register', array($this, 'registerCustomize'));
        }
    }

    /**
     * Get the service name
     */
    public function getName() : string
    {
        return $this->config->getName();
    }

    /**
     * Get the REST base path for the service
     * @return [type] [description]
     */
    public function getRestBasePath() : string
    {
        return $this->config->getRestBasePath();
    }

    /**
     * Register a custom post typ
     * @param  string     $postType     the custom post type
     * @param  string     $slug         the url slug
     * @param  string     $singularName the singular nam
     * @param  string     $pluralName   the plural name
     * @param  array|null $args         optional arguments
     */
    public function registerPostType(string $postType, string $slug, string $singularName, string $pluralName, ?array $args = null, ?array $support = null, $icon = 'dashicons-palmtree')
    {
        // Add author post type support
        if($this->config->getAuthorSupport()) {
            add_post_type_support($this->config->getPostType(), 'author');
        }

        $singularNameLower = strtolower($singularName);
        $pluralNameLower = strtolower($pluralName);

        $labels = array(
            'name'               => _x($pluralName, 'post type general name', 'insight' ),
            'singular_name'      => _x($singularName, 'post type singular name', 'insight' ),
            'menu_name'          => _x($pluralName, 'admin menu', 'insight' ),
            'name_admin_bar'     => _x($singularName, 'add new on admin bar', 'insight' ),
            'add_new'            => _x('Add New', $singularName, 'insight' ),
            'add_new_item'       => __('Add New ' . $singularName, 'insight' ),
            'new_item'           => __('New ' . $singularName, 'insight' ),
            'edit_item'          => __('Edit ' . $singularName, 'insight' ),
            'view_item'          => __('View ' . $singularName, 'insight' ),
            'all_items'          => __('All ' . $pluralName, 'insight' ),
            'search_items'       => __('Search ' . $pluralName, 'insight' ),
            'parent_item_colon'  => __('Parent ' . $pluralName . ':', 'insight' ),
            'not_found'          => __('No ' . $pluralNameLower . ' found.', 'insight' ),
            'not_found_in_trash' => __('No ' . $pluralNameLower . ' found in Trash.', 'insight' )
        );

        $args = $args ?: array(
            'labels' => $labels,
            'description' => __('Description.', 'yunik' ),
            'public' => true,
            'show_in_rest' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => $slug, 'with_front' => true),
            'capability_type' => 'post',
            'has_archive' => false,
            'hierarchical' => false,
            'menu_position' => null,
            'menu_icon' => $icon,
            'supports' => $support ?: array('title', 'editor')
        );
        
        register_post_type($postType, $args);
    }

    public function getConfig() : WpServiceConfig
    {
        return $this->config;
    }

    protected function unexpectedApiError(BaseException $ex)
    {
        return rest_ensure_response(array());
    }

    protected function assertRequestFields(\WP_REST_Request $request, array $fields, $throwIfMissing = false)
    {
        $invalid = [];

        foreach ($fields as $field) {
            $val = $request->get_param($field);
            if(empty($val)) {
                $invalid[] = $field;
            }
        }
        $isInvalid = count($invalid) > 0;

        if($isInvalid && $throwIfMissing) {
            ValidatorException::throw($invalid);
        }

        return $isInvalid;
    }


    /**
     * Trigger a hook
     * @source  https://gist.github.com/unnikked/fe48953e1605a3ab78b9
     * @param  string $name   [description]
     * @param  array  $params [description]
     * @return [type]         [description]
     */
    public function triggerHook(string $name, array $params = [])
    {
        if(!isset($this->hooks[$name])) {
            return $this;
        }

        $queue = $this->hooks[$name];
        $queue->top();
        
        // process the queue
        while($queue->valid()){
            $action = $queue->current();
            if (is_callable($action)){
                if (call_user_func_array($action, $params) === false) {
                    YunikLogger::error("Error triggering the hook $name");
                    break; // stop propagation on error
                }
            }
            $queue->next();
        }
        
        return $this;
    }
    
    /**
     * Register a hooks
     * @param  string  $name     [description]
     * @param  [type]  $callback [description]
     * @param  integer $priority [description]
     * @return [type]            [description]
     */
    public function onHook(string $name, $callback, $priority = 0)
    {
        if(!isset($this->hooks[$name])) {
            $this->hooks[$name] = new \SplPriorityQueue();  
        }

        if (is_object($callback) && $callback instanceof \Closure) {
            $callback = $callback->bindTo($this, $this);
        }
        // add the event to the priority queue
        $this->hooks[$name]->insert($callback, $priority);

    }
}
<?php

namespace Yunik\Wp;

use Yunik\BaseException,
	Yunik\Interfaces\CacheProviderInterface,
	Yunik\Cache\YunikCacheProvider,
	Yunik\Sync\SyncManager,
	Yunik\Logger\YunikLogger,
	Yunik\Db\LocalPdo,
	Yunik\Interfaces\AppInterface,
	Yunik\Wp\WpServiceAbstract;




/**
 * Abstract application to run the Sync plugin with diferent implementations
 *
 * Each implementation is responsible to fetch and handle the data by their own
 */
abstract class WpApp implements AppInterface {

    const COUNTER_MODIFIED = 'modified_counter';

    const LAST_MODIFIED_KEY = 'last_modified';

    public $cache;

    /**
     * Plugin data from get_plugin_data()
     * @var [type]
     */
    public $pluginData;

    /**
     * Prefix permits to run more than one version of this plugin in the same WP installation
     * Used to define slugs, database keys and others options
     * @var string
     */
    protected $prefix;

    /**
     * Name used for this plugin
     * @var string
     */
	protected $name;

	protected $webServer;

	/**
	 * Default settings defined when the plugin is activated
	 */
	const SETTINGS_DEFAULTS = [
		
	];

    public function __construct($pluginData = null, $prefix = 'yunik')
    {
    	$this->pluginData = $pluginData;
    	$this->prefix = strtolower($prefix) . '_';
    	$this->name = $prefix;
    }

    /**
     * Return the prefix for this application
     *
     * Prefix is used to maintain two or more versions of the same plugin
     * executing in the same WP installation
     * @return string the prefix
     */
    public function getPrefix() : string
    {
    	return $this->prefix;
    }

    /**
     * Return the name for this application
     * @return string the name
     */
    public function getName() : string
    {
    	return $this->name;
    }

    /**
     * Return the cache component
     * @return CacheProviderInterface the cache component
     */
    public function getCache() : CacheProviderInterface
    {
    	return $this->cache;
    }

    /**
     * Register all required hooks
     * @return [type] [description]
     */
    public function registerHooks() : void
    {
    	// Register actions
    	add_action('wp_dashboard_setup', array($this, 'registerDashboardWidget'));
		add_action('admin_menu', array($this, 'addSettingsPage'));
		add_action('admin_bar_menu', array($this, 'registerAdminBar'), 100);
		add_action('rest_api_init', array($this, 'registerApiHooks'));
		add_action('cmb2_admin_init', array($this, 'createMetaboxes'));
		add_action('admin_enqueue_scripts', array($this, 'registerAdminScripts'));
		add_action('wp_enqueue_scripts', array($this, 'registerScripts'), 20);
		add_action('wp_trash_post', array($this, 'onDeletePost'));


		// Register flters
		add_filter('plugin_action_links_' . PLUGIN_LINKS_PATH, array($this, 'addPluginSettingsLink'));
		add_filter('cron_schedules', array($this, 'cronSchedules'));

		// Check for dirty products
		if (!wp_next_scheduled('rcsoft_cron_hook') ) {
		    wp_schedule_event(time(), '1min', 'rcsoft_cron_hook');
		}
		add_action('rcsoft_cron_hook', array($this, 'cronExecute'));

		register_activation_hook(PLUGIN_ROOT, array($this, 'setupPlugin'));
    }

    /**
     * Invoked when a post is deleted
     * @param  int $postId the post ID
     */
    public function onDeletePost($postId)
    {
    	$post = get_post($postId);

    	// Post should never be null as it was trashed, not deleted. Even so..
    	if($post === null) {
    		YunikLogger::error('Post with ID ' . $postId . ' not found.');
    		return;
    	}

    	// If we can't write then something is wrong
    	if(!is_writable(PLUGIN_REDIRECT_FILE)) {
    		YunikLogger::error("Failed to add a link to the redirect list. The file isn\'t writtable, located at: " . PLUGIN_REDIRECT_FILE);
    		return;
    	}

    	if($post->post_type !== 'product') {
    		return;
    	}

    	$url = get_permalink($postId);

    	$rule = 'rewrite ' . $url . ' ' . $this->cache->get(self::SETTINGS_DEFAULTS . 'rewrite');
    	$file = fopen(PLUGIN_REDIRECT_FILE, 'a');
    	fwrite($file, $rule . "\n");
    	fclose($file);
    }

    /**
     * Reload the web server configuration
     */
    public function reloadWebServer() : void
    {
    	// Ignore user aborts, otherwise PHP script will stop executing at this point
    	ignore_user_abort(true);

    	switch ($this->webServer) {
    		case 'nginx':
    			$this->reloadNginx();
    			break;

    		default:
    			throw new \Exception("Plugin only supports Nginx webserver. Detected: " . $this->webServer);
    			break;
    	}
    }

    /**
     * Reload the nginx configuration
     */
    public function reloadNginx() : void
    {
    	exec(PLUGIN_ROOT . '/includes/reload-nginx.sh');
    }

    /**
     * Invoked when the plugin is activated
     */
    public function setupPlugin() : void
    {
    	$keys = $this->setupDefaultSettings();
    	YunikLogger::debug(count($keys) > 0 ? 'Default keys: ' . print_r($keys) : 'No default keys saved');

    	if(!$this->isInstalled()) {
    		$this->installPlugin();
    	}
    }

    /**
     * Indicates if the plugin was already configured
     * @return boolean [description]
     */
    public function isInstalled() : bool
    {
    	return $this->cache->get(self::PREFIX . 'installed') ?? false;
    }


    /**
     * Configure the plugin with the default settings
     */
    public function setupDefaultSettings() : array
    {
    	// Keep track of which defaults were created
    	$keys = [];
    	foreach (self::SETTINGS_DEFAULTS as $key => $default) {
    		// None of settings should be empty
    		if(empty($this->cache->get(self::SETTINGS_PREFIX . $key))) {
    			$this->cache->set(self::SETTINGS_PREFIX . $key, $default);
    			$keys[] = $key;
    		}
    	}

    	return $keys;
    }

	/**
	 * Add a new cron history
	 * @param string $time time elapsed
	 * @param string $type the operation type, ie: fetch, queue
	 */
	protected function addCronHistory($time, $type)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare(self::QUERY_INSERT_HISTORY);
		$stmt->bindParam(':duration', $time);
		$stmt->bindParam(':type', $type);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		$pdo = null;

		if(!$res) {
			YunikLogger::error("Error creating a new history.");
			return false;
		}
	}

	/**
	 * Return a list of cron histories
	 * @return array array containing cron histories
	 */
	protected function getCronHistory()
	{
		$pdo = new LocalPdo();

		$stmt = $pdo->prepare(self::QUERY_SELECT_HISTORY);

		$stmt->execute();
		$res = $stmt->fetchAll();

		$pdo = null;
		return $res;
	}

    /**
     * Helper method to echo a list of result with a table
     * @param  array  $headers  the table headers, ie: array('Id', 'ProductId', 'Sku')
     * @param  array  $messages the table tbody, ie: array(array(1, 2, 'sku_1'), array(2, 3, 'sku_2'))
     * @return [type]           [description]
     */
    protected function echoResultTable(array $headers, array $messages)
    {
    	$output = '<table class="wp-list-table widefat fixed striped"><thead><tr>';
    	foreach ($headers as $header) {
    		$output .= '<td>' . $header . '</td>';
    	}

    	$output .= '</tr></thead><tbody>';

    	foreach ($messages as $message) {
    		$output .= '<tr>';
    		foreach ($message as $entry) {
    			if(!is_string($entry)){
    				$entry = $this->parseValueAsString($entry);
    			}
    			$output .= "<td>$entry</td>";
    		}
    		$output .= '</tr>';
    	}

    	$output .= '</tr></tbody></table>';
    	return $output;
    }

    /**
     * Helper method to format values into string
     * @param  mixed $value the value
     * @return string        a readable version of the value
     */
    private function parseValueAsString($value) : string
    {
    	if(is_array($value)) {
    		return implode(', ', $value);
    	}

    	return print_r($value, true);

    }

    /**
     * Calculate the time elapsed from a two periods
     *
     * Start end End parameters can be obtained with microtime(true)
     * and to return as float
     * @source https://stackoverflow.com/questions/7850259/calculate-elapsed-time-in-php
     * @param  float  $start the start
     * @param  float  $end   the end
     * @return string        the time in hh:mm:ss
     */
    protected function calculateTimeElapsed(float $start, float $end) : string
    {
    	$timeDiff = $end - $start;
    	$h = floor($timeDiff / 3600);
		$timeDiff -= $h * 3600;
		$m = floor($timeDiff / 60);
		$timeDiff -= $m * 60;
		return $h.':'.sprintf('%02d', $m).':'.sprintf('%02d', $timeDiff);
    }
}
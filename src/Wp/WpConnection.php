<?php

declare(strict_types=1);

namespace Yunik\Wp;

use Yunik\Db\LocalPdo,
	Yunik\Logger\Logger;




/**
 * Wrapper to use LocalPdo to connect to WP
 */
class WpConnection {

	/**
	 * Wordpress hostname connection
	 * @var string
	 */
	private $hostname;

	/**
	 * Wordpress database user
	 * @var string
	 */
	private $user;

	/**
	 * Wordpress database password
	 * @var string
	 */
	private $password;

	/**
	 * Wordpress database tables prefix
	 * @var string
	 */	
	private $prefix;

	/**
	 * Use a socke to connect to MySQL server
	 * @var bool
	 */
	private $useSocket;

	/**
	 * The database name
	 * @var string
	 */
	private $dbName;

	/**
	 * PDO object used
	 * @var LocalPdo
	 */
	protected $pdo = null;

	/**
	 * The WpConnection singleton intance 
	 * @var self
	 */
	protected static $instance = null;

	public function __construct(string $hostname, string $user, string $password, ?string $prefix = null, $useSocket = false)
	{
		$this->hostname = $hostname;
		$this->user = $user;
		$this->password = $password;
		$this->prefix = $prefix;
		$this->useSocket = $useSocket;
		$this->dbName = DB_NAME;
	}

	/**
	 * Clear the WP connection component
	 */
	public static function clear() : void
	{
		self::$instance = null;
	}

	/**
	 * Configure and initialize the instance
	 * @param  string $hostname Wordpress database hostname
	 * @param  strign $user     Wordpress database user
	 * @param  string $password Wordpress database passqord
	 * @param  string $prefix   Wordpress database tables prefix
	 * @param  string $useSocket Use a socket to connect to MySQL server
	 */
	public static function configure($hostname, $user, $password, $prefix = null, $useSocket = false) : void
	{
		if(self::$instance !== null) {
			Logger::debug("WpConnection is already configured. Cleaning the previous configuration.");
		}

		self::$instance = new WpConnection($hostname, $user, $password, $prefix, $useSocket);
	}

	/**
	 * Return the singleton of this component
	 * @return WpConnection the WpConnection instance
	 */
	public static function instance() : self
	{
		if(self::$instance === null) {
			throw new WpConnectionNotConfiguredException();
		}

		return self::$instance;
	}

	/**
	 * Return the PDO object by this component to connect
	 * to a existing Wordpress site
	 */
	public function getPdo() : LocalPdo
	{
		if($this->pdo === null) {
			LocalPdo::config($this->hostname, $this->dbName, $this->user, $this->password, $this->useSocket);
			$this->pdo = new LocalPdo();
		}

		return $this->pdo;
	}

	/**
	 * Return the Wordpress database hostname
	 */
	public function getHostname() : string
	{
		return $this->hostname;
	}

	/**
	 * Return the Wordpress database username
	 */
	public function getUser() : string
	{
		return $this->user;
	}

	/**
	 * Return the Wordpress database password
	 */
	public function getPassword() : string
	{
		return $this->password;
	}

	/**
	 * Return the prefix used for Wordpress database tables
	 */
	public function getPrefix() : string
	{
		return $this->prefix;
	}
}
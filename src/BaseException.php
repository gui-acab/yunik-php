<?php

declare(strict_types=1);

namespace Yunik;




/**
 * Base Exception
 */
class BaseException extends \Exception {

	public function __construct(string $message = 'Internal Error', int $code = 0)
	{
		parent::__construct($message, $code);
	}

	static function create(\Exception $ex, $message = 'Internal Error')
	{
		if($ex instanceof \PDOException)
			return self::createPdoException($ex, $message);

		return $ex;
	}
	
	static function createPdoException(\Exception $ex, $message = 'Internal Error')
	{
		switch ($ex->getCode()) {
			case 2002:
				return new self("Database is offline");
			
			default:
				return $ex;
		}
	}
}
<?php

declare(strict_types=1);

namespace Yunik\Cache;




class YunikCacheProvider {
	
	protected static $provider;

	static function config(AbstractCacheProvider $provider)
	{
		self::$provider = $provider;	
	}

	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	static function get(string $key, $default = null)
	{
		return self::$provider->get($key, $default);
	}

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	static function set(string $key, $value) : void
	{
		self::$provider->set($key, $value);
	}

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	static function incrBy($key, $by = 1) : int
	{
		self::$provider->incrBy($key, $by);
	}
}
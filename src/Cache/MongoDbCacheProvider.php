<?php

namespace Yunik\Cache;

use Yunik\BaseException;




/**
 * Cache Provider implementation using MongoDB
 */
class MongoDbCacheProvider extends AbstractCacheProvider {

	static $client;

	static $col;

	const CACHE_ID = 'mongodb';

	const CACHE_COL_KEY = 'caching';

	static function config($client)
	{
		self::$client = $client;
		self::$col = $client->test->dics;
	}

	/**
	 * Get the ID for the cache provider implementation
	 * @return string the ID
	 */
	public function getId() : string
	{
		return self::CACHE_ID;
	}
	
	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	public function get(string $key, $default = null)
	{
		$res = self::$col->findOne(['_id' => self::CACHE_COL_KEY]);

		if(!$res)
			return $default;

		return $res[$key];
	}

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	public function set(string $key, $value) : void
	{
		$doc = $this->getOrCreateDoc();
		self::$col->updateOne(
			$this->findDocFilder(),
			[ '$set' => [ $key => $value]]
		);
	}

	protected function createDoc()
	{
		return self::$col->insertOne($this->findDocFilder);
	}

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	public function incrBy($key, $by = 1) : int
	{
		self::$col->updateOne(
			$this->findDocFilder(),
			['$inc' => [$key => $by]]
		);

		return $this->get($key);
	}

	/**
	 * Get or create the cache document
	 */
	protected function getOrCreateDoc()
	{
		$doc = self::$col->findOne($this->findDocFilder());
		if(!$doc) {
			$doc = $this->createDoc();
		}
		return $doc;
	}

	protected function findDocFilder()
	{
		return ['_id' => self::CACHE_COL_KEY];
	}
}
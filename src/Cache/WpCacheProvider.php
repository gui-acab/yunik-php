<?php

namespace Yunik\Cache;

use Yunik\Db\LocalPdo;




/**
 * Cache Provider implementation using Wordpress
 * 
 * This provider doesn't relly on Wordpress existence
 * instead use PDO
 */
class WpCacheProvider extends AbstractCacheProvider {

	const CACHE_ID = 'wordpress';

	const CACHE_PREFIX = 'rcsoft_';

	/**
	 * Get the ID for the cache provider implementation
	 * @return string the ID
	 */
	public function getId() : string
	{
		return self::CACHE_ID;
	}
	
	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	public function get(string $key, $default = null)
	{
		$pdo = new LocalPdo();
		$stmt = $pdo->prepare('SELECT option_id, option_name, option_value FROM wp_options WHERE option_name=:name');
		$keyName = self::CACHE_PREFIX . $key;
		$stmt->bindParam(':name', $keyName);
		$stmt->execute();

		$res = $stmt->fetch();

		$pdo = null;
		if(!is_array($res) || !array_key_exists('option_value', $res)) {
			return $default;
		}

		return $res['option_value'];
	}

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	public function set(string $key, $value) : void
	{
		$pdo = new LocalPdo();
		$keyName = self::CACHE_PREFIX . $key;

		$this->exists($pdo, $keyName)
			? $this->update($pdo, $keyName, $value)
			: $this->insert($pdo, $keyName, $value);

		$pdo = null;
	}

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	public function incrBy($key, $by = 1) : int
	{
		$v = $this->get($key, 0);
		$v = $v + $by;
		$this->set($key, $v);

		return $v;
	}

	/**
	 * Indicates if a option key is already saved
	 * @param  LocalPdo &$pdo PDO instance
	 * @param  string $key  the key name
	 * @return bool       True if option exists, false otherwise
	 */
	protected function exists(&$pdo, $key) : bool
	{
		$stmt = $pdo->prepare('SELECT count(*) FROM wp_options WHERE option_name=:name');
		$stmt->bindParam(':name', $key);

		$stmt->execute();
		$res = $stmt->fetch();

		return $res[0] > 0;
	}

	/**
	 * Insert a option/value
	 * @param  LocalPdo &$pdo  PDO instance
	 * @param  string $key   the key name
	 * @param  mixed $value the value
	 */
	protected function insert(&$pdo, $key, $value) : void
	{
		$autoload = false; // LATER get it from settings
		
		$stmt = $pdo->prepare('INSERT INTO wp_options (option_name, option_value, autoload) VALUES(:name, :value, :autoload)');
		$stmt->bindParam(':name', $key);
		$stmt->bindParam(':value', $value);
		$stmt->bindParam(':autoload', $autoload);

		$res = $stmt->execute();
		$id = $pdo->lastInsertId();
		
		$pdo = null;
		if(!$res) {
			throw new BaseException("Error setting $key with value $value");
		}
	}

	/**
	 * Update a option/value
	 * @param  LocalPdo &$pdo  PDO instance
	 * @param  string $key   the key name
	 * @param  mixed $value the value
	 */
	protected function update(&$pdo, $key, $value) : void
	{
		$stmt = $pdo->prepare('UPDATE wp_options SET option_value=:value WHERE option_name=:name');
		$stmt->bindParam(':name', $key);
		$stmt->bindParam(':value', $value);

		$res = $stmt->execute();

		if(!$res) {
			throw new BaseException("Error setting $key with value $value");
		}
	}
}
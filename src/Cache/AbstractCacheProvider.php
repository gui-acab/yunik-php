<?php

namespace Yunik\Cache;

use Yunik\Interfaces\CacheProviderInterface;




/**
 * The abstract implementation for the Cache Provider
 */
abstract class AbstractCacheProvider implements CacheProviderInterface {

	/**
	 * Get the ID for the cache provider implementation
	 * @return string the ID
	 */
	abstract function getId() : string;

	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	abstract function get(string $key, $default = null);

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	abstract function set(string $key, $value) : void; 

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	abstract function incrBy($key, $by = 1) : int;
}
<?php

namespace Yunik\Cache;

use Yunik\BaseException;




/**
 * Cache Provider implementation using Memcached
 */
class MemcachedCacheProvider extends AbstractCacheProvider {

	static $client;

	const CACHE_ID = 'memcached';

	static function config($client)
	{
		self::$client = $client;
		self::$client->addServer('localhost', 11211);
		self::$client->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
	}

	/**
	 * Get the ID for the cache provider implementation
	 * @return string the ID
	 */
	public function getId() : string
	{
		return self::CACHE_ID;
	}
	
	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	public function get(string $key, $default = null)
	{
		$res = self::$client->get($key);

		if($res === null)
			return $default;

		return $res;
	}

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	public function set(string $key, $value) : void
	{
		self::$client->set($key, $value);
		if(self::$client->getResultCode() !== 0) {
			throw new BaseException("Error in memcached");
		}
	}

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	public function incrBy($key, $by = 1) : int
	{
		self::$client->increment($key, $by, $by);
		return self::$client->get($key);
	}
}
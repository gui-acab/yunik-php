<?php

namespace Yunik\Cache;




/**
 * Cache Provider implementation using Redis
 */
class RedisCacheProvider extends AbstractCacheProvider {

	static $client;

	const CACHE_ID = 'redis';

	static function config($client)
	{
		self::$client = $client;
		self::$client->connect('localhost', 6379);
	}

	/**
	 * Get the ID for the cache provider implementation
	 * @return string the ID
	 */
	public function getId() : string
	{
		return self::CACHE_ID;
	}
	
	/**
	 * Retrives a option value based on a option key name
	 * @param  string $key     the key name
	 * @param  mixed $default default value to be returned if not exists
	 * @return mixed          the option value or default if not null
	 */
	public function get(string $key, $default = null)
	{
		$res = self::$client->get($key);

		if(!$res)
			return $default;

		return $res;
	}

	/**
	 * Update a option/value pair
	 * @param string $key   the key name
	 * @param mixed $value the value
	 */
	public function set(string $key, $value) : void
	{
		self::$client->set($key, $value);
	}

	/**
	 * Increment by a value
	 * @param  string  $key the key name
	 * @param  integer $by  incremented by
	 * @return int       the total
	 */
	public function incrBy($key, $by = 1) : int
	{
		return self::$client->incr($key, $by);
	}
}
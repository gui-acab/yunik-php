Yunik PHP Library
====================

v0.1.154

PHP Library developed internally @Yunik.

Main components:
- Logger
- PDO implementation
- Wordpress Options 


## Instalation

composer require yunik/yunik-php

## Info

For more information contact webdesign@yunik.us

<?php

declare(strict_types=1);

namespace Tests\Wp;

use PHPUnit\Framework\TestCase,
	Yunik\Wp\WpConnection,
	Yunik\Wp\Options,
	Yunik\Utils;




class OptionsTest extends TestCase {

	private $testKey;

	private $testValue;

	public function setUp()
	{
		WpConnection::configure(DB_HOSTNAME, DB_USER, DB_PASSWORD, null, true);
		$this->testKey = Utils::randomString(20);
		$this->testValue = Utils::randomString(20);
	}

	/**
	 * Test that can set and then get a option by the key
	 */
	public function testCanSetAndGetOption() : void
	{
		Options::configure(WpConnection::instance());
		Options::set($this->testKey, $this->testValue);
		$value = Options::get($this->testKey);

		$this->assertEquals($this->testValue, $value);
	}

	/**
	 * Test that can set a option and then removing it
	 * returning null
	 */
	public function testCanSetAndDeleteAOption() : void
	{
		Options::configure(WpConnection::instance());
		Options::set($this->testKey, $this->testValue);
		Options::delete($this->testKey);
		$value = Options::get($this->testKey);

		$this->assertNull($value);
	}
}
<?php

declare(strict_types=1);

namespace Tests\Wp;

use PHPUnit\Framework\TestCase,
	Yunik\BaseException,
	Yunik\Wp\WpServiceConfig,
	Yunik\Wp\WpServiceAbstract,
	Yunik\Wp\WpKernelModule,
	Yunik\Interfaces\WpServiceOnPostSave,
	Yunik\Interfaces\WpServiceOnPostTrash,
	Yunik\Interfaces\WpServiceOnPostDelete,
	Yunik\Interfaces\WpCustomizeRegister;




class MockService extends WpServiceAbstract implements WpServiceOnPostSave, WpServiceOnPostTrash, WpServiceOnPostDelete, WpCustomizeRegister{

	private $registerScriptsInvoked = false;

	private $registerAdminScriptsInvoked = false;
	
	private $registerApiHooksInvoked = false;

	private $registerCustomizeInvoked = false;

	private $onSaveInvoked = false;

	private $onTrashInvoked = false;

	private $onDeleteInvoked = false;

	private $invocations = ['registerScripts', 'registerAdminScripts', 'registerApiHooks', 'registerCustomize', 'onSave',  'onTrash', 'onDelete'];

	public function __construct()
	{
		parent::__construct(new WpServiceConfig(
			__('Doctor', 'cssm'),
			__('Doctors', 'cssm'), 
			'doctor', 
			'doctor',
			'doctor'
		));
	}

	public function registerScripts() : void
	{
		$this->registerScriptsInvoked = true;
	}

    public function registerAdminScripts() : void
    {
    	$this->registerAdminScriptsInvoked = true;
    }

    public function registerApiHooks() : void
    {
    	$this->registerApiHooksInvoked = true;
    }

    public function registerCustomize(\WP_Customize_Manager $customize) : void
    {
    	$this->registerCustomizeInvoked = true;
    }

	public function onSave(int $postId, \Wp_Post $post, bool $update)
	{
		$this->onSaveInvoked = true;
	}

	public function onTrash(int $postId)
	{
		$this->onTrashInvoked = true;
	}

	public function onDelete(int $postId)
	{
		$this->onDeleteInvoked = true;
	}

	public function assertInvocations()
	{
		$errors = false;
		foreach ($this->invocations as $methodName) {
			if($this->{$methodName . 'Invoked'} === false) {
				$errors[] = $methodName;
			}
		}

		if($errors !== false) {
			throw new \Exception("One or more expected method was not invoked. " . print_r($errors, true));
		}
	}

	public function getCheckDoctorAvailability(\WP_REST_Request $request)
	{
		try {
			return rest_ensure_response(array('ok' => true));
		}
		catch(BaseException $ex) {
			return $this->unexpectedApiError($ex);
		}
	}
}

class WpKernelModuleTest extends TestCase {

	private $service;

	public function setUp()
	{
		$this->service = new MockService();
	}

	public function testMethodsAreInvoked()
	{
		//$this->service->assertInvocations();
	}
}
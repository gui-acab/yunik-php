<?php

namespace Tests\Wp;

use PHPUnit\Framework\TestCase,
	Yunik\Wp\WpConnection,
	Yunik\Wp\WpConnectionNotConfiguredException;




class WpConnectionTest extends TestCase {

	public function tearDown()
	{
		WpConnection::clear();
	}

	/**
	 * Assert that WpConnection act as a singleton object
	 * 
	 * The instance method should only create a instance once
	 * and return it when invoked
	 */ 
	public function testObjectActAsASingleton() : void
	{
		$this->configureWp();
		$instance = WpConnection::instance();
		$sameInstance = WpConnection::instance();

		$this->assertSame($instance, $sameInstance);

		$newInstance = new WpConnection('/Applications/MAMP/tmp/mysql/mysql.sock', 'root', 'root', true);

		$this->assertEquals($instance, $newInstance);
		$this->assertNotSame($instance, $newInstance);
	}

	/**
	 * Assert that WpConnection must be configured before
	 * it can be used with instance method
	 */ 
	public function testThrowsExceptionIfNotConfigured() : void
	{
		$throwed = false;
		try {
			WpConnection::instance();
		} catch (WpConnectionNotConfiguredException $e) {
			$throwed = true;
		}

		$this->assertTrue($throwed);
	}

	/**
	 * Assert that WpConnection only creates one instance
	 * of LocalPdo
	 */
	public function testCreateAndReturnSameInstanceOfPdo() : void
	{
		$this->configureWp();
		$pdo = WpConnection::instance()->getPdo();
		$samePdo = WpConnection::instance()->getPdo();

		$this->assertSame($pdo, $samePdo);
	}

	/**
	 * Clear must remove the instance
	 */
	public function testCanClearInstance() : void
	{
		$throwed = false;
		$this->configureWp();
		WpConnection::instance(); // here it's already assigned
		WpConnection::instance()->clear();

		try {
			WpConnection::instance();
		} catch (WpConnectionNotConfiguredException $e) {
			$throwed = false;
		}

		$this->assertTrue($throwed);
	}

	private function configureWp() : void
	{
		WpConnection::configure(DB_HOSTNAME, DB_USER, DB_PASSWORD, null, DB_SOCKET);
	}
}
<?php

namespace Tests\Cache;

use PHPUnit\Framework\TestCase,
	Yunik\Cache\MongoDbCacheProvider;




class MongoDbCacheProviderTest extends AbstractCacheProviderTest {

	public function setUp()
	{
		MongoDbCacheProvider::config(new \MongoDB\Client());
		$this->cache = new MongoDbCacheProvider();
		$this->testKey = rand(1, 100) . 't' . rand(2, 200);
	}
}
<?php

namespace Tests\Cache;

use PHPUnit\Framework\TestCase,
	Yunik\Cache\WpCacheProvider;




/**
 * Abstract implementation to test Cache components
 * 
 * Required tests are implemented that only the provider
 * must be set at the setUp and also the testKey
 */
abstract class AbstractCacheProviderTest extends TestCase {

	protected $cache;

	protected $testKey;

	public function testCanSetAndGetOption()
	{
		$key = rand(1, 1000);
		$this->cache->set('avc', $key);

		$val = $this->cache->get('avc');
		$this->assertEquals($key, $val);
	}

	public function testCanUpdateAndGetExistingOption()
	{
		$key = rand(1, 100);
		$this->cache->set('avc', $key);
		$val = $this->cache->get('avc');
		$this->assertEquals($key, $val);

		$newVal = rand(101, 201);
		$this->cache->set('avc', $newVal);
		$val = $this->cache->get('avc');
		$this->assertEquals($newVal, $val); 
	}

	public function testCanIncrementWithOneAsDefault()
	{
		$this->cache->incrBy($this->testKey);
		$val = $this->cache->get($this->testKey);

		$this->assertEquals($val, 1);
	}

	public function testCanIncrementWithDefaultValues()
	{
		$this->cache->incrBy($this->testKey, 10);
		$val = $this->cache->get($this->testKey);

		$this->assertEquals($val, 10);
	}
}
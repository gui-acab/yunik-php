<?php

namespace Tests\Cache;

use PHPUnit\Framework\TestCase,
	Yunik\Cache\MemcachedCacheProvider;




class MemcachedCacheProviderTest extends AbstractCacheProviderTest {

	public function setUp() : void
	{
		MemcachedCacheProvider::config(new \Memcached());
		$this->cache = new MemcachedCacheProvider();
		$this->testKey = rand(1, 100) . 't' . rand(2, 200);
	}
}

<?php

namespace Tests\Cache;

use PHPUnit\Framework\TestCase,
	Yunik\Cache\RedisCacheProvider;




class RedisCacheProviderTest extends AbstractCacheProviderTest {

	public function setUp()
	{
		RedisCacheProvider::config(new \Redis());
		$this->cache = new RedisCacheProvider();
		$this->testKey =rand(1, 100) . 't' . rand(2, 200);
	}
}
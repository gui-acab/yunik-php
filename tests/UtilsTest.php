<?php

namespace Tests;

use PHPUnit\Framework\TestCase,
	Yunik\Utils;




class UtilsTest extends TestCase {

	protected $table = array(
		array('minimum' => 2, 'price' => 5),
		array('minimum' => 5, 'price' => 10),
		array('minimum' => 10, 'price' => 15),
		array('minimum' => 20, 'price' => 50)
	);

	public function testCanGenerateRandomString() : void
	{
		// Save all generated strings to compare later
		$generated = [];

		for ($i=0; $i < 5; $i++) { 
			$str = Utils::randomString(50);
			$this->assertFalse(in_array($str, $generated)); // generated string must be unique
			
			$generated[] = $str;
			$this->assertTrue(strlen($str) === 50); // assert string length
		}
	}

	public function testDontApplyDiscountIfMinimumIsntMatched()
	{
		$discount = $this->getDiscountFor(1);;
		$this->assertTrue($discount == 0);
	}

	public function testGetDiscountForSameNumber()
	{
		$discount = $this->getDiscountFor(2);;
		$this->assertTrue($discount === 5);

		$discount = $this->getDiscountFor(10);;
		$this->assertTrue($discount === 15);
	}

	public function testCanGetLastDiscountForMorePeople()
	{
		$discount = $this->getDiscountFor(30);;
		$this->assertTrue($discount === 50);
	}

	public function testGetDiscountBetweenTwoValues()
	{
		$discount = $this->getDiscountFor(3);;
		$this->assertTrue($discount === 5, "Expected 5 got $discount");

		$discount = $this->getDiscountFor(6);
		$this->assertTrue($discount === 10);

		$discount = $this->getDiscountFor(11);
		$this->assertTrue($discount === 15, "Expected 15 got $discount");

	}

	protected function getDiscountFor($people)
	{
		return Utils::getDiscountFromTable($this->table, $people);
	}

	public function testCanValidateValidVatNumbers()
	{
		$valid = Utils::validateVat('47458714', 'dk');
		$this->assertTrue($valid);
	}
}
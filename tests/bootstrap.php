<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Yunik\Logger\YunikLogger,
	Yunik\Db\LocalPdo,
	Yunik\Logger\WpLogger;




YunikLogger::config(new WpLogger());
LocalPdo::config(DB_HOSTNAME, DB_NAME, DB_USER, DB_PASSWORD, DB_SOCKET);

if(!function_exists('__')) {
	function __($message, $domain)
	{
		return $message;
	}
}

if(!function_exists('add_action'))
{
	function add_action($name, $callback)
	{
		
	}
}
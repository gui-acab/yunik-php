<?php

namespace Tests;

use PHPUnit\Framework\TestCase,
	Yunik\Sync\SyncManager,
	Yunik\Domain\WpProductDto;


class SyncProductTest extends TestCase {

	private $filepath;

	private $skuTest = '5604429651871';

	private $sync;

	private $mockExternalId;

	private $mockproductId;

	private $mockProduct;

	private $mockProductSku = 'bbbb';

	private $mockCategoryId;

	public function setUp()
	{
		$this->sync = new SyncManager();
		$this->rebuildMockProduct();
		//$this->sync->clearproductLists();
	}

	protected function rebuildMockProduct()
	{
		$this->mockExternalId = rand(1, 500);
		$this->mockproductId = rand(100, 1000);
		$this->mockCategoryId = rand(200, 1000);
		$this->mockProductSku = $this->mockproductId;
		$this->mockProduct = new WpProductDto($this->mockproductId, $this->mockExternalId, $this->mockProductSku, 10, 100, date("Y-m-d H:i:s"));
	}

	public function tearDown()
	{
		//$this->sync->clearproductLists();
	}

	public function testCanGetProductByQueueItemId()
	{
		$res = $this->sync->addProductToPending($this->mockProduct);
		$id = $res->getQueueItemId();

		$queueItem = $this->sync->getProductFromQueue($id);
		$this->assertEquals($queueItem['id'], $id);
	}

	public function testCanCheckIfProductIsEqualWithoutList()
	{
		// Without list
		$this->sync->addProductToPending($this->mockProduct);
		$equal = $this->sync->productIsEqual($this->mockProduct);
		
		$this->assertTrue($equal);

		// Pending list, should exist
		$equal = $this->sync->productIsEqual($this->mockProduct, SyncManager::LIST_PENDING);
		$this->assertTrue($equal);

		// Failed list, shouldn't exist
		$equal = $this->sync->productIsEqual($this->mockProduct, SyncManager::LIST_FAILED);
		$this->assertFalse($equal);		
	}

	public function testCanCheckIfProductIsEqual()
	{
		$date = date("Y-m-d H:i:s");

		$this->sync->addProductToPending($this->mockProduct);
		$equal = $this->sync->productIsEqual($this->mockProduct, SyncManager::LIST_PENDING);
		$this->assertTrue($equal);

		// Different list
		$equal = $this->sync->productIsEqual($this->mockProduct, SyncManager::LIST_FAILED);
		$this->assertFalse($equal);

		// Different id
		$other = new WpProductDto(1, 'aaa', 100, 10, 150);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		// Different SKU
		$other = new WpProductDto($this->mockExternalId, 'a', 10, 100, 50);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		// Different price
		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 101, 50);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		// Different price promo
		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 100, 51);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		// Different price promo start date
		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 100, 50, $date);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		// Different price promo end date
		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 100, 50, null, $date);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);


		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 100, 50, null, $date, 'new-category');
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);	
	}

	public function testCanCheckIfProductIsEqualWithCategory()
	{
		$this->sync->addProductToPending($this->mockProduct);
				// Different category
		$other = new WpProductDto($this->mockExternalId, $this->mockProductSku, 10, 100, 50, null, date("Y-m-d H:i:s"), $this->mockCategoryId);
		$equal = $this->sync->productIsEqual($other, SyncManager::LIST_PENDING);
		$this->assertFalse($equal);

		$this->rebuildMockProduct();
		$this->mockProduct->setCategory($this->mockCategoryId);

		$this->sync->addProductToPending($this->mockProduct);
		$equal = $this->sync->productIsEqual($this->mockProduct, SyncManager::LIST_PENDING);
		$this->assertTrue($equal);
	}

	public function testCanclearproductLists() {
		$this->sync->addProductToPending($this->mockProduct);
		$count = $this->sync->countProducts(SyncManager::LIST_PENDING);
		$this->assertTrue($count > 0);

		$this->sync->clearProductLists();
		$count = $this->sync->countProducts(SyncManager::LIST_PENDING);
		$this->assertTrue($count == 0);
	}
	
	public function testAddproductToPendingAndRetrieve()
	{
		$res = $this->sync->addProductToPending($this->mockProduct);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertTrue($exists);
	}

	public function testAddproductToFailedAndRetrieve()
	{
		$res = $this->sync->addProductToFailed($this->mockProduct);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->productIsFailed($this->mockExternalId);
		$this->assertTrue($exists);
	}

	public function testAddproductToPendingThenFailed()
	{
		$res = $this->sync->addProductToPending($this->mockProduct);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertTrue($exists);
		
		$exists = $this->sync->productIsFailed($this->mockExternalId);
		$this->assertFalse($exists);

		$res = $this->sync->addProductToFailed($this->mockProduct);
		$this->assertTrue($res->isOk());

		$res = $this->sync->removeProductFromPending($this->mockExternalId);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertFalse($exists);
	}

	public function testAddproductToPendingThenMarkAsSuccess()
	{
		// Add the product to the Pending list to be removed later
		$res = $this->sync->addProductToPending($this->mockProduct);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertTrue($exists);

		$res = $this->sync->markProductAsCreated($this->mockExternalId);
		$this->assertTrue($res->isOk());

		// Should be removed from the Pending list
		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertFalse($exists);
	}

	public function testMarkproductAsFailed()
	{
		// Add to Pending list to be removed before inserted into Failed
		$res = $this->sync->addProductToPending($this->mockProduct);
		$this->assertTrue($res->isOk());

		// Should exists in the Pending list
		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertTrue($exists);

		$res = $this->sync->markProductAsFailed($this->mockExternalId);
		$this->assertTrue($res->isOk());

		// The product should be removed from the Pending list
		$exists = $this->sync->productIsPending($this->mockExternalId);
		$this->assertFalse($exists);

		$exists = $this->sync->productIsFailed($this->mockExternalId);
		$this->assertTrue($exists);
	}

	public function testGetPendingProducts()
	{
		$f = new WpProductDto($this->mockExternalId + 1, $this->mockProductSku, 123);
		$this->sync->addProductToPending($f);
		$list = $this->sync->getPendingProducts(0, 100);
		$count = count($list);
		$res = $this->sync->addProductToPending($this->mockProduct);
		$this->assertTrue($res->isOk());

		// Now the list must be greather than before, one if not concurrency...
		$list = $this->sync->getPendingProducts(0, 100);
		$this->assertTrue(count($list) > $count);
	}

	public function testGetFailedProducts()
	{
		$list = $this->sync->getFailedProducts(0, 100);
		$count = count($list);
		$res = $this->sync->addProductToFailed($this->mockProduct);
		$this->assertTrue($res->isOk());

		$res = $this->sync->productIsFailed($this->mockExternalId);
		$this->assertTrue($res);

		// Now the list must be greather than before, one if not concurrency...
		$list = $this->sync->getFailedProducts(0, 100);
		$this->assertTrue(count($list) > $count);
	}
}
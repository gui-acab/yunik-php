<?php

namespace Tests;

use PHPUnit\Framework\TestCase,
	Yunik\Utils,
	Yunik\Sync\Sync;




class SyncTest extends TestCase
{

	private $mockKey = '5604429651871';

	private $mockList = 'test';
	private $sync;


	public function setUp()
	{
		$this->sync = new Sync();
		$this->mockKey = rand(1, 500);

	}

	public function testCanRemoveList()
	{
		$res = $this->insertMock();
		$count = count($this->sync->query($this->mockList));
		$this->assertTrue($count > 1);

		$this->sync->removeList($this->mockList);
		$count = count($this->sync->query($this->mockList));
		$this->assertTrue($count == 0);
	}

	public function testCheckIfItemExists()
	{
		$this->insertMock();
		$exists = $this->sync->exists($this->mockList, $this->mockKey);
		$this->assertTrue($exists);
	}

	public function testCanQueryItems()
	{
		$this->insertMock();
		$items = $this->sync->query($this->mockList);
		$this->assertTrue(count($items) > 0);
	}

	public function testCanQueryExpiredItems()
	{
		$count = count($this->sync->queryExpired($this->mockList));

		$until = date("Y-m-d H:i:s", strtotime('now - 1 day'));
		$this->insertMock('rand', $until);

		$items = $this->sync->queryExpired($this->mockList);
		
		$this->assertTrue(count($items) == $count+1);
	}

	private function insertMock($value = 'rand', $until = null)
	{
		return $this->sync->insert($this->mockList, $this->mockKey, $value, $until);
	}
}
<?php

namespace Tests;

use PHPUnit\Framework\TestCase,
	Yunik\Utils,
	Yunik\Sync\SyncManager,
	Yunik\Domain\WpCategoryDto;;


class SyncCategoryTest extends TestCase {

	private $filepath;

	private $skuTest = '5604429651871';

	private $sync;

	private $mockExternalId;

	private $mockExternalName;

	private $mockCategoryId;

	private $mockCategory;


	public function setUp()
	{
		$this->sync = new SyncManager();
		$this->mockExternalId = rand(1, 500);
		$this->mockCategoryId = rand(500, 1000);
		$this->mockExternalName = Utils::randomString();
		$this->mockCategory = new WpCategoryDto($this->mockExternalId, $this->mockExternalName);
		$this->sync->clearCategoryLists();
	}

	public function tearDown()
	{
		
	}

	public function testCanclearCategoryLists() {
		$this->sync->addCategoryToPending($this->mockCategory);
		$count = $this->sync->countCategories(SyncManager::LIST_PENDING);
		$this->assertTrue($count > 0);

		$this->sync->clearCategoryLists();
		$count = $this->sync->countCategories(SyncManager::LIST_PENDING);
		$this->assertTrue($count == 0);
	}
	
	public function testAddCategoryToPendingAndRetrieve()
	{
		$res = $this->sync->addCategoryToPending($this->mockCategory);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertTrue($exists);
	}

	public function testAddCategoryToFailedAndRetrieve()
	{
		$res = $this->sync->addCategoryToFailed($this->mockCategoryId);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->categoryIsFailed($this->mockCategoryId);
		$this->assertTrue($exists);
	}

	public function testAddCategoryToPendingThenFailed()
	{
		$res = $this->sync->addCategoryToPending($this->mockCategory);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertTrue($exists);
		
		$exists = $this->sync->categoryIsFailed($this->mockExternalId);
		$this->assertFalse($exists);

		$res = $this->sync->addCategoryToFailed($this->mockCategoryId);
		$this->assertTrue($res->isOk());

		$res = $this->sync->removeCategoryFromPending($this->mockExternalId);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertFalse($exists);
	}

	public function testAddCategoryToPendingThenMarkAsSuccess()
	{
		// Add the category to the Pending list to be removed later
		$res = $this->sync->addCategoryToPending($this->mockCategory);
		$this->assertTrue($res->isOk());

		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertTrue($exists);

		$res = $this->sync->markCategoryAsCreated($this->mockCategoryId, $this->mockExternalId);
		$this->assertTrue($res->isOk());

		// Should be removed from the Pending list
		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertFalse($exists);
	}

	public function testMarkCategoryAsFailed()
	{
		// Add to Pending list to be removed before inserted into Failed
		$res = $this->sync->addCategoryToPending($this->mockCategory);
		$this->assertTrue($res->isOk());

		// Should exists in the Pending list
		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertTrue($exists);

		$res = $this->sync->markCategoryAsFailed($this->mockExternalId);
		$this->assertTrue($res->isOk());

		// The category should be removed from the Pending list
		$exists = $this->sync->categoryIsPending($this->mockExternalId);
		$this->assertFalse($exists);

		$exists = $this->sync->categoryIsFailed($this->mockExternalId);
		$this->assertTrue($exists);
	}

	public function testGetPendingCategories()
	{
		$this->sync->addCategoryToPending($this->mockCategory);
		$list = $this->sync->getPendingCategories(0, 100);
		$count = count($list);

		$res = $this->sync->addCategoryToPending($this->mockCategory);
		$this->assertTrue($res->isOk());

		// Now the list must be greather than before, one if not concurrency...
		$list = $this->sync->getPendingCategories(0, 100);
		$this->assertTrue(count($list) > $count);
	}

	public function testGetFailedCategories()
	{
		$list = $this->sync->getFailedCategories(0, 100);
		$count = count($list);
		$res = $this->sync->addCategoryToFailed($this->mockCategoryId);
		$this->assertTrue($res->isOk());

		$res = $this->sync->categoryIsFailed($this->mockCategoryId);
		$this->assertTrue($res);

		// Now the list must be greather than before, one if not concurrency...
		$list = $this->sync->getFailedCategories(0, 100);
		$this->assertTrue(count($list) > $count);
	}
}
<?php

namespace Tests;

use PHPUnit\Framework\TestCase,
	Yunik\Sync\SyncManager,
	Yunik\Domain\WpProductDto;


class SyncProductMapTest extends TestCase {

	private $sync;

	private $woocommerceId;

	private $rcsoftId;

	public function setUp()
	{
		$this->sync = new SyncManager();
		$this->woocommerceId = rand(1, 500);
		$this->rcsoftId = rand(100, 1000);
	}

	public function tearDown()
	{
		//$this->sync->clearproductLists();
	}

	public function testCanAddProductMapAndGetById()
	{
		$res = $this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$this->assertTrue(is_int($res));

		$map = $this->sync->getProductMap($res);
		$this->assertEquals($res, $map['id']);
	}

	public function testCanAddProductMapAndGetByWoocommerceId()
	{
		$res = $this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$map = $this->sync->getProductMapByWoocommerce($this->woocommerceId);
		$this->assertEquals($res, $map['id']);
	}

	public function testCanAddProductMapAndGetByRcsoftId()
	{
		$res = $this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$map = $this->sync->getProductMapByRcsoft($this->rcsoftId);
		$this->assertEquals($res, $map['id']);
	}

	public function testCanAddAndRemoveProductMap()
	{
		$res = $this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$this->sync->removeProductMap($this->woocommerceId, $this->rcsoftId);
		$map = $this->sync->getProductMap($res);
		$this->assertFalse($map);
	}

	public function testCanCheckIfProductMapExists()
	{
		$this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$exists = $this->sync->productMapExists($this->woocommerceId, $this->rcsoftId);
		$this->assertTrue($exists);

		// Remove the product
		$this->sync->removeProductMap($this->woocommerceId, $this->rcsoftId);
		$exists = $this->sync->productMapExists($this->woocommerceId, $this->rcsoftId);
		$this->assertFalse($exists);
	}

	public function testCanCheckIfProductMapExistsByRscoft()
	{
		$this->sync->addProductMap($this->woocommerceId, $this->rcsoftId);
		$exists = $this->sync->productMapExistsByRcsoft($this->rcsoftId);
		$this->assertTrue($exists);

		// Remove the product
		$this->sync->removeProductMap($this->woocommerceId, $this->rcsoftId);
		$exists = $this->sync->productMapExistsByRcsoft($this->rcsoftId);
		$this->assertFalse($exists);
	}
}
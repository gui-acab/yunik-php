<?php

namespace Tests\Logger;

use PHPUnit\Framework\TestCase,
	Yunik\Logger\TextFileLogger;


class TextFileLoggerTest extends TestCase {

	private $filepath;

	public function setUp() : void
	{
		$this->filepath = tempnam('/tmp', 'rcsoft');
		TextFileLogger::config($this->filepath);
	}
	
	public function testCanWriteOnTemporaryFile() : void
	{
		$content = 'Test message';
		TextFileLogger::log($content);
		
		$exists = file_exists($this->filepath);
		$this->assertTrue($exists);

		$file = file_get_contents($this->filepath);
		$formated = 'LOG: ' . $content . PHP_EOL;
		$this->assertEquals($file, $formated);
	}
}
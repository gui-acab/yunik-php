<?php

namespace Tests\Logger;

use PHPUnit\Framework\TestCase,
	Yunik\Logger\Logger;




class LoggerTest extends TestCase {

	public function setUp() : void
	{
		
	}

	/**
	 * Assert that the logger can write to the console
	 * using ob_ methods to capture the output
	 */
	public function testCanWriteToConsole() : void
	{
		$message = 'Writing to console';
		ob_start();
		Logger::log($message);

		$output = ob_get_contents();
		ob_end_clean();
		
		$this->assertEquals($message, $output);
	}
}